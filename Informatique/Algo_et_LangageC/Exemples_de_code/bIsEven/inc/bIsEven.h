/* bIsEven.h */

#ifndef __B_IS_EVEN_H__
#define __B_IS_EVEN_H__

#include <stdbool.h>


void initialize_rand( void );

bool bIsOdd (int);

void display_result(int);

void test_parity(int);

#endif  /*  _B_IS_EVEN_HH__ */
