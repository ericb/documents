/*                                                   *
 * Programme exemple d'utilisation d'une structure   *
 * pour exprimer les coordonnees d'un point          *
 * Auteur : Eric Bachard  version originale : 2004   *
 * Ce document est sous Licence GPL v2               *
 * voir : http://www.gnu.org/licenses/gpl-2.0.html   */

#include <stdio.h>
#include <stdlib.h>

#include "point.h"

int
main( void)
{
    /* struct Point abscisse; */
    POINT point1;
    point1.x = 3.0;
    point1.y = 1.2;
   
   POINT position = { 10.0, 10.0};

   fprintf( stdout,
            " Coordonnées du point trouvé : %.2lf (en x) et %.2lf (en y) \n",
                                                point1.x, point1.y );

   fprintf( stdout, "Coordonnées de la position : %.2lf , %.2lf \n", position.x, position.y);
   fprintf( stdout, "Coordonnées du point1 avant modification : %.2lf , %.2lf \n", point1.x, point1.y);
   point1.x = 4.0;
   point1.y = 1.1;
   fprintf( stdout, "Coordonnées du point1 après modification : %.2lf , %.2lf \n", point1.x, point1.y);
   return EXIT_SUCCESS;
}

