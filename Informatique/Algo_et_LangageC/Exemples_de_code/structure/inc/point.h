/* fichier d'en-tête 
   point.h : contient
   la définition 
   de la structure POINT
*/

#ifndef _POINT_H
#define _POINT_H

typedef struct Point {
    double x;
    double y;
} POINT;

#endif   /* _POINT_H */