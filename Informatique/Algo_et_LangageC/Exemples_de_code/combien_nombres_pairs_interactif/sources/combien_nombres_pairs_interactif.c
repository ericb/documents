#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <saisie.h>
#include <efface_ecran.h>

int compteur = 0;
int min = 0;
int max = 0;

static short int saisie_bornes()
{
    efface_ecran();
    fprintf( stdout, "Saisir un nombre entier positif ou nul entre zéro et 32765 : ");
    min = saisie_nombre_entier_court(0, 32765);
    fprintf(stdout, "\n");

    do
    {
        fprintf( stdout, "Saisir un nombre supérieur à %d \n", min );

        max = saisie_nombre_entier_court(0, 32765);
    }
    while (max < min);


    return EXIT_SUCCESS;
}


int main (void)
{
    int anError = saisie_bornes();

    if (anError != 0)
        fprintf(stderr, "Pb with saisie_bornes\n");

    int indice = 0;
    for ( indice = min ; indice <= max; indice++ )
    {
        if ( (indice % 2) == 0)
             compteur++;
    }

    efface_ecran();
    fprintf( stdout,
             "Il y a %d nombres pairs entre %d et %d \n",
             compteur,
             min,
             max
           );

    return EXIT_SUCCESS;
}


