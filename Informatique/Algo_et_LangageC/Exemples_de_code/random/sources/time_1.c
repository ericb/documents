#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>  /* gettimeofday() */
#include <unistd.h>    /* CLOCK_REALTIME */
#include <time.h>      /*    timespec    */


#define MIN_NUMBER  0
#define MAX_NUMBER 20

#include "saisie.h"

/* C structures containing a given time in microsecond, and more */
struct timeval tv;
struct timespec tspec;

char g_buf[MAX_BUFFER_SIZE];

int main(int argc,char** argv)
{
    long int utime1 = 0;
    clock_gettime(CLOCK_REALTIME, &tspec);
#ifdef DEBUG
    fprintf(stdout, "Time with tspec : %ld \n", tspec.tv_nsec);
#endif
    gettimeofday(&tv, NULL);
    utime1 = tv.tv_usec;
    srand(utime1);

    fprintf(stdout, "Combien de nombres tirés au sort ? (compris entre %d et %d)\n", MIN_NUMBER, MAX_NUMBER);

    int nombre_de_tirages = saisie_nombre_entier_court(MIN_NUMBER, MAX_NUMBER);

    for(int i=0 ; i<nombre_de_tirages ; i++)
        fprintf(stdout, "%d\n", rand()%1000 + 1);

    return 0;
}


