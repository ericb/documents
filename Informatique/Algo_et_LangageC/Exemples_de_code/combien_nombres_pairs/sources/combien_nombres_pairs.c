#include <stdio.h>
#include <stdlib.h>
//#include <math.h>

#define MIN 10
#define MAX 13

int main (void)
{
  int compteur = 0;
  int indice = 0; 
  for ( indice = MIN ; indice <= MAX; indice++ )
  {
      if ( indice % 2 == 0)
           compteur++;
  }
  fprintf( stdout,
           "Il y a %d nombres pairs entre %d et %d \n",
           compteur,
           MIN,
           MAX
         );

  return EXIT_SUCCESS;
}

