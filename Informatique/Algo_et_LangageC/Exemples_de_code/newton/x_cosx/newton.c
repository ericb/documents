#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double newton( double);
double fonction( double);


double newton( double value)
{
    return (value - (value - cos(M_PI*value/180.0f))/(1 - sin(M_PI*value/180.0f)));
}

double fonction( double value)
{
    return (value - cos(M_PI*value/180.0f));
}

const double epsilon = 1.e-10;

double value = 1.4f;
double xn_1 = 0.0;

int main(void)
{
   xn_1 = newton(value);
   int counter = 1;

   while(fabs(fonction(xn_1)) > epsilon)
   {
     xn_1 = newton(xn_1);
     counter++;
   }

   fprintf(stdout, "La racine vaut :  %.10f, calculée en %d itérations\n", xn_1*180.0f/M_PI, counter);

   return EXIT_SUCCESS;
}

