#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double newton( double);
double fonction( double);


double newton( double value)
{
    return ((value*value+16.0f)/(2*value+1.0f));
}

double fonction( double value)
{
    return ((value*value + value - 16.0f)/(2*value+1.0f));
}


const double epsilon = 1.e-6;

double value = 5.0f;
double xn_1 = 0.0;

int main(void)
{
   xn_1 = newton(value);
   int counter = 1;

   while(fabs(fonction(xn_1)) > epsilon)
   {
     xn_1 = newton(xn_1);
     counter++;
   }

   fprintf(stdout, "La racine vaut :  %f, calculée en %d itérations\n", xn_1, counter);

   return EXIT_SUCCESS;
}

