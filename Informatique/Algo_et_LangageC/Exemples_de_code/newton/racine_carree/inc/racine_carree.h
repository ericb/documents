/* algorithme de Newton, appliqué au calcul d'une racine carrée
 * fichier d'en-tête racine.h
 * Eric Bachard / mars 2016 / Licence LGPL
 */

#ifndef  __RACINE_CARREE_H__
#define  __RACINE_CARREE_H__

typedef struct calcul {
    int compteur ;
    long double epsilon;
    long double racine_cherchee;
    long double xn;
    long double erreur;
} CALCUL_RACINE_CARREE;

/* déclaration précede */
void saisie_valeurs(CALCUL_RACINE_CARREE *);

void calcul_racine_carree(CALCUL_RACINE_CARREE *);


#endif  /*  __RACINE_CARREE_H__ */
