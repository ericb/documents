/* algorithme de Newton, appliqué au calcul d'une racine carrée (version 2, utilisant structure + pointeur)
 * Eric Bachard / mars 2016 / Licence LGPL
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <saisie.h>
#include "racine_carree.h"


int main(void)
{
  CALCUL_RACINE_CARREE     r = { 1, 1e-9, 0.0 , 0.0, 0.0 };

  CALCUL_RACINE_CARREE * p_r = &r;

  saisie_valeurs(p_r);
  calcul_racine_carree(p_r);

  fprintf (stdout, "La racine carrée de %Lf vaut %.10Lf. Résultat obtenu en %d iterations (erreur %.12Lf)  \n", p_r->racine_cherchee, r.xn, r.compteur, r.erreur);

  return EXIT_SUCCESS;
}

