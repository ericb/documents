/* algorithme de Newton, appliqué au calcul d'une racine carrée
 * Eric Bachard / mars 2016 / Licence LGPL
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <saisie.h>

#define EPSILON  1e-9


int main(void)
{
  int compteur = 1;
  long double erreur = 0.0;

  fprintf( stdout, "Donner la valeur de la racine carrée à calculer (comprise entre 0.0 et 1e6) : ");
  long double racine_cherchee = saisie_nombre_reel(0.0, 1e6);

  fprintf( stdout, "Donner la valeur de départ (comprise entre 0.0 et 1e5 : ");
  long double xn = saisie_nombre_reel(0.0, 1e5 );

  while (fabs(racine_cherchee - (xn * xn)) > EPSILON)
  {
    erreur = fabs( sqrt(racine_cherchee) - xn);

    xn = 0.50*( xn + (racine_cherchee / xn));
    fprintf( stderr, "compteur vaut : %d ; erreur : %3.12Lf \n", compteur, erreur);
    ++compteur;
  }
  fprintf (stdout, "La racine carrée de %Lf  vaut %.10Lf. Résultat obtenu en %d iterations (erreur %.12Lf)  \n", racine_cherchee, xn, compteur, erreur);

  return EXIT_SUCCESS;
}
