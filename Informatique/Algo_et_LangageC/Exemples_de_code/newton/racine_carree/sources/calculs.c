/* algorithme de Newton, appliqué au calcul d'une racine carrée (version 2, utilisant structure + pointeur)
 * Eric Bachard / mars 2016 / Licence LGPL
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <saisie.h>
#include "racine_carree.h"

void saisie_valeurs(CALCUL_RACINE_CARREE * p_r /* adresse de r */)
{
  fprintf( stdout, "Donner la valeur de la racine carrée à calculer (comprise entre 0.0 et 1e6) : ");
  p_r->racine_cherchee = saisie_nombre_reel(0.0, 1e6);

  fprintf( stdout, "Donner la valeur de départ (comprise entre 0.0 et 1e5 : ");
  p_r->xn = saisie_nombre_reel(0.0, 1e5 );
}


void calcul_racine_carree(CALCUL_RACINE_CARREE * p_r)
{
  while (fabs(p_r->racine_cherchee - (p_r->xn * p_r->xn)) > p_r->epsilon)
  {
    p_r->erreur = fabs( sqrt(p_r->racine_cherchee) - p_r->xn);

    p_r->xn = 0.50*( p_r->xn + (p_r->racine_cherchee / p_r->xn));
    fprintf( stderr, "compteur vaut : %d ; erreur : %3.12Lf \n", p_r->compteur, p_r->erreur);
    ++p_r->compteur;
  }
}
