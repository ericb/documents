#define _GNU_SOURCE

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include <sys/time.h>  /* gettimeofday() */
#include <unistd.h>    /* CLOCK_REALTIME */
#include <time.h>      /*    timespec    */

/* C structures containing a given time in microsecond, and more */
struct timeval tv;
struct timespec tspec;


int main(void)
{
  long int utime1 = 0;
  clock_gettime(CLOCK_REALTIME, &tspec);
#ifdef DEBUG
  fprintf(stdout, "Time with tspec : %ld \n", tspec.tv_nsec);
#endif
  gettimeofday(&tv, NULL);
  utime1 = tv.tv_usec;
  srand(utime1);

  for(int i=0;i<10;i++)
    fprintf(stdout, "%d\n",rand()%488);

  return 0;
}


