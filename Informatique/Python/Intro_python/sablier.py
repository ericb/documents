#!/usr/bin/python

# on charge la bibliothèque turtle et toutes ses fonctions en mémoire
from turtle import *

# rectangle épais

width(6)
color(0.2, 0.2, 0.2)
goto(60,0)
goto(60,110)
goto(0,110)
goto(0,0)

# Déplacement

up()
goto(5,5)
down()

# Dessin du sablier gris clair

width(1)
fillcolor("grey")
begin_fill()
goto(55, 5)
goto(5, 105)
goto(55, 105)
goto(5, 5)

end_fill()

read()
clickonexit()


