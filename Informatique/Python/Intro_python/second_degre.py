import math

# definition globale des coefficients du polynôme
a  = 0.
b  = 0.
c  = 0.

def delta(a,b,c):
    return b*b-4*a*c

def saisie_parametres():
    global a, b, c
    va=input("Entrer a : ")
    a=float(va)
    vb=input("Entrer b : ")
    b=float(vb)
    vc=input("Entrer c : ")
    c=float(vc)
    return

def calcul_solution():
    # debug
    print("L'équation à résoudre est : ", a, "x^2 + ", b, "x +", c)

    if (delta(a,b,c) < 0):print("pas de solution dans R")
    elif (delta == 0): print("solution unique ", -b/(2*a))
    else:
        print("Cette équation possède 2 solutions : ")
        racine1 = (-b - math.sqrt(delta(a,b,c)))/(2*a)
        racine2 = (-b + math.sqrt(delta(a,b,c)))/(2*a)
        print(racine1, racine2)
    return


# /* debut */

saisie_parametres()
calcul_solution()


