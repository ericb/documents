import numpy as np
from matplotlib import pyplot as plt

T = 150
omega = 1.4
#omega = 2.1
lamda = 0.03

def euler(N,x0,v0):
    x = x0
    v = v0
    h = T/N
    print("h = ",h)
    a = -2*lamda*v-omega*omega*x

    tab_1 = np.zeros(N)
    tab_2 = np.zeros(N)

    for i in range(N):
        (x,v,a)=(x+v*h,v+a*h,-2*lamda*v-omega*omega*x)
        tab_1[i] = x
        tab_2[i] = v

    print("tab_1] contient : ",tab_1)
    print("tab_2] contient : ",tab_2)

    return tab_1

def temps(N):
    h=T/N
    t = np.arange(N)*h
    return t
    
plt.figure ('graphique')
plt.title("representation graphique de r en fonction de t")
print("euler(3,0,1) retourne : ",euler(3,0,1))
plt.plot(temps(15000),euler(15000,0,1),'b')
plt.xlabel("t")
plt.ylabel("r")
plt.grid()
plt.show()
