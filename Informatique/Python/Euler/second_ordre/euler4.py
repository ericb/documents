import numpy as np
from matplotlib import pyplot as plt

T=150
#omega = 1.4
lamda = 0.03

def euler(N,x0,v0):
    x = x0
    v = v0
    h = T/N
    a = -2*lamda*v-omega*omega*x
    tab_1 = np.zeros(N)
    tab_2 = np.zeros(N)
    i = 0

    while (i < N):
        (x,v,a)=(x+v*h,v+a*h,-2*lamda*v-omega*omega*x)
        tab_1[i] = x
        tab_2[i] = v
        ++i

    return tab_1

def temps(N):
    h=T/N
    t = np.arange(N)*h
    return t

# période T = 2.1 s  => omega = (2*pi) / T
# 
omega = 3

def vraie(N,v0):
    x=0
    v=v0
    h=T/N
    tab_3=temps(N)

    for i in range(N):
        tab_3[i]=np.sin(np.sqrt(omega*omega-lamda*lamda)*h*i)*np.exp(-lamda*h*i)*v/(np.sqrt(omega*omega-lamda*lamda))

    return tab_3

plt.figure ('graphique')
plt.plot(temps(15000),vraie(15000,1),'b')
plt.title("représentation graphique de r en fonction de t")
plt.xlabel("t")
plt.ylabel("r")
plt.grid()
plt.show()