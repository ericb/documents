#!/bin/bash

function action_fichier()
{
    local reponse
    local saisie

    echo "*********************"
    PS3="
    action sur $1 :"

    select reponse in Informations Copier Deplacer Detruire Retour
    do
        case $reponse in
            Informations)
                echo
		ls -l $1
	        echo
                ;;
            Copier)
                echo -n "copier $1 vers?"
                if ! read saisie;then continue; fi
                cp $1 $saisie
                ;;
            Deplacer)
                echo -n "nouvel emplacement pour $1"
                if ! read saisie; then continue; fi
                mv $1 $saisie
                break
                ;;
    	    Detruire)
                if rm -i $1; then break; fi
                ;;
            Retour)
                break
                ;;
            *)
                if [ "$REPLY" ="0" ]; then break ; fi
		echo "$REPLY n'est pa une reponse valide"
                echo
                ;;
        esac
    done
}
 
function liste_fichiers()
{
 
    echo "***********************"
    PS3="fichier a traiter:"
    select fichier in *
    do
        if [ ! -z "$fichier" ] ; then
            action_fichier $fichier
#                  return 0
        fi
            if [ "$REPLY" = "0" ] ; then
		return 1
            fi
	    echo "==>entrez 0 pour quitter"
	    echo
    done
}
 
while liste_fichiers ; do : ; done 

