#!/bin/bash

set -u
set -x
set -v

# une illustration des boucles imbriquees Auteur : Eric Bachard 09/2004

repertoire_courant=`pwd`
repertoire1=modified_version
repertoire2=official_tree
echo -e "Que souhaitez-vous faire ?\n"
echo -e "Creer l'arborescence (entrer 1)\n"
echo -e "Effacer l'arborescence ? (entrer 2)"

read reponse

case "$reponse" in
    1) 
    for arbo in $repertoire1 $repertoire2 ; do
    mkdir $arbo ;
    cd $arbo ; 
	for (( i=1 ; i<11 ; i++ )); do 
	    for (( j=1 ; j<6 ; j++ )); do 
		for ((k=1 ; k<4 ; k++)) ; do
		    mkdir -p ./rep$i/sous_rep$j/sous_sous_rep$k ; 
		    cd ./rep$i/sous_rep$j/sous_sous_rep$k ; 
		    for (( l=1; l<6 ; l++ )) ; do 
			touch fichier$l ;
		    done
		    cd ../../.. ;
		done 
	    done
	done
    cd .. ;
    done
    echo -e "deux arborescences de 150 repertoires, comportant chacune 750 fichiers ont ete correctement creees \n"
    ;;
    2) 
    cd $repertoire_courant
    rm -rf $repertoire_courant/$repertoire1 $repertoire_courant/$repertoire2
    ;;
    *)
    echo -e "Valeur erronee..\n"
    ;;
esac

exit 16

