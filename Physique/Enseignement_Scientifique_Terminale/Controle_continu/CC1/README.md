## Information

Ce dossier contient les sujets et les corrigés des contrôles continus de sciences physiques de la matière enseignement scientifique portant sur le thème 1, pour l'année 2021-2022.

Deux formats de fichiers sont proposés : **.pdf** (lecture surtout, facilité d'utilisation) et **.odt** : fichiers sources (modifiables).

**La licence d'utilisation est la licence [Creative Commons by-sa](https://creativecommons.org/licenses/by-sa/2.0/fr/)**. En cas de besoin d'une autre licence, merci de me contacter : voir **[page d'accueil à la racine](https://framagit.org/ericb/documents)** et bien faire dérouler la page. Il suffit de savoir lire entre les lignes.

## Organisation de l'arborescence

Les épreuves n'ayant pas pu se dérouler simultanément pour les 5 classes, chaque sujet correspond à une ou plusieurs classes.

* sujet 1  : **terminale B**, **terminale C** et **terminale D**

* sujet 2  : **terminale A**

* sujet 3 : **terminale E**


## Retours et questions

Le barème proposé est donné à titre d'information seulement, et a pu ête modifié ou adapté à la classe. Malgré le soin apporté à la rédaction de ces documents qui sont mis en ligne gracieusement, il est possible qu'il reste quelques fautes de frappe, ou erreurs. Le cas échéant, merci pour votre retour constructif, et ces problèmes seront corrigés dès que possible.

**Rappel** : les cours et documents complémentaires correspondant au premier thème sont disponibles **[ICI](https://framagit.org/ericb/documents/-/tree/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer)** dans les dossiers **[introduction](https://framagit.org/ericb/documents/-/tree/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer/introduction)** et **[ressources](https://framagit.org/ericb/documents/-/tree/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer/introduction)**. Voir en particulier le document  **[synthèse 2021](https://framagit.org/ericb/documents/-/blob/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer/ressources/synthese_2021_12_20.pdf)**



**Eric Bachard**

**Lycée de la Mer et du Littoral**


