## Information

Ce dossier contient les sujets et les corrigés des contrôles continus de sciences physiques de la matière enseignement scientifique portant sur le thème 2, pour l'année 2021-2022.

Deux formats de fichiers sont proposés : **.pdf** (lecture surtout, facilité d'utilisation) et **.odt** : fichiers sources (modifiables).

**Sauf mention contraire, la licence d'utilisation pour tous ces documents est la licence [Creative Commons by-sa](https://creativecommons.org/licenses/by-sa/2.0/fr/)**. En cas de besoin d'une autre licence, merci de me contacter : voir **[page d'accueil à la racine](https://framagit.org/ericb/documents)** et bien faire dérouler la page. Il suffit de savoir lire entre les lignes.

- Terminale A : sujet 6

- Terminales B,C et D : sujet 4

- Terminale E : sujet 5

## Organisation de l'arborescence

(À compléter)

## Retours et questions

Le barème proposé est toujours donné à titre d'information seulement (il peut être modifié ou adapté à la classe sans préavis)

Malgré le soin apporté à la rédaction de ces documents qui sont mis en ligne gracieusement, il est possible qu'il reste quelques fautes de frappe, ou erreurs. Le cas échéant, merci pour votre retour constructif, et ces problèmes seront corrigés dès que possible.

**Rappel** : les cours et documents complémentaires correspondant au second thème sont disponibles **[ICI](https://framagit.org/ericb/documents)** (à compléter)



**Eric Bachard**

**Lycée de la Mer et du Littoral (Bourcefranc-le-Chapus 17560)**


