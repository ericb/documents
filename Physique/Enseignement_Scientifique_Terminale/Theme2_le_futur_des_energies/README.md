# Contenu de cette page

Tout ce que vous trouverez dans ce dossier (et sous-dossiers) concerne l'enseignement scientifique de physique en terminale.

Certains documents sont une copie des originaux, car c'est plus simple de chercher à un seul endroit. 

Toutefois, je vous encourage à visiter les liens originaux, car cela en vaut vraiment la peine, et valorise le travail original.


Sauf changement récent, les liens donnés sur cette page sont valides. Merci de me signaler tout problème.

Eric Bachard
février 2022

# Thèmes abordés

Il sont conformes au programme officiel paru au Bulletin Officiel de l'Éducation Nationale (BOEN), que vous trouverez dans le sous-dossier documents utiles.

Le Thème traité ici est intitulé : **le futur des énergies**, et nous traiterons les items suivants :

- item 1.4 : Énergie, choix de développement et futur climatique

- item 2.1 : Deux siècles d'énergie électrique

- item 2.2 : Les atouts de l'électricité

- item 2.3 : Optimisation du transport de l'électricité

- item 2.5 : Choix énergétiques et impacts sur les sociétés

# Plan proposé

## Séance 1   Les énergies

### 1.1 Rappels de ce qui a été fait précédemment :

- une histoire de l'atmosphère terrestre
- le mécanisme de l'effet de serre

**[Lien vers la synthèse du thème 3](https://framagit.org/ericb/documents/-/blob/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer/ressources/synthese_2021_12_20.pdf)**

**[Lien direct de la vidéo de Jean-Louis Dufresne (numéroté 1)](https://www.ac-paris.fr/portail/jcms/p2_1982654/modelisation-de-l-effet-de-serre-par-jean-louis-dufresne)**


### 1.2 Introduction : [un immeuble de poupées (cf le conte de Pierre André Mangin)](https://www.energie-environnement.ch/fichiers/contes/conte_immeuble_poupees.pdf)

- Que faut-il retenir de ce conte ?

### 1.3   Une petite histoire de la maîtrise de l'énergie et de ses conversions

**[Lien direct vers le contenu de la présentation](https://framagit.org/ericb/documents/-/blob/master/Physique/Enseignement_Scientifique_Terminale/Theme2_le_futur_des_energies/Seance1/Enseignement_Scientifique_Terminale_Theme_2_Energies.pdf)**

Cette partie a été inspirée par le site d'EdF : **[Les grandes dates de l'électricité](https://www.edf.fr/groupe-edf/espaces-dedies/l-energie-de-a-a-z/les-grandes-dates-de-l-electricite)**


Points clés :

* notion de transfert technologique d'une invention
* l'essor du transport
* les espoirs et les limites
    - le cas de la fusion thermonucléaire. **[Les dernières nouvelles ICI](https://www.bbc.com/news/science-environment-60312633)**
    - le problème de la consommation
    - la distribution de l'énergie consommée : ratio d'énergie renouvelable / non-renouvelable


Complément : petites vidéos sur l'induction


## Séance 2  Approche dimensionnelle de l'énergie

### 2.1 Outils pour quantifier l'énergie

Questionnement : quelle est la dimension d'une énergie ? Et d'une puissance ?

- analyse dimensionnelle : construction des grandeurs physiques de référence
- régles d'homogénéité dans l'écriture scientifique
- unités utilisées
- conversions et calculs types à connaître

Exercices dirigés par groupe, avec présentation des résultats par groupe

Conclusion avec quelques ordres de grandeur d'énergies et de puissances à connaître

## Séance 3  Rendement et ordres de grandeur des énergies utilisées

### 3.1  Manipuler les dimensions

Exercices dirigés sur le thème de l'utilisation des règles d'homogénéité dans les équations et la vérification de la justesse d'un résultat littéral.

### 3.2  Bilan des précédentes séances

Évaluation en auto-correction. Durée : 30 min


## Séance 4  Rendement et ordres de grandeur des énergies utilisées

### 4.1   Énergie ou énergies ?

De quoi parle-t-on ?

- Énergie macroscopiques, microscopiques mises en jeu dans les phénomènes observés
- Exemples d'énergies mises en jeu en fonction du domaine d'étude, **du point de vue du physicien**

### 4.2  Les différentes formes de énergie,

- Les propriétés de l'énergie et représentation associée

- Notion de transfert énergétique

- Énergies renouvelables et non renouvelables

### 4.3  Analyse documentaire

- états des lieux sur l'énergie nucléaire en France (d'après EdF)

- comparaison avec les énergies renouvelables (d'après données 2021, Ministère de la Transition Écologique) sous forme de travail en groupes, avec communication des conclusions par groupe


## Séance 5  Rendement et transfert maximal en puissance

**Représentation des conversions d'énergie**

- Notion de rendement

- Diagrammes de Sankey

- Exemples de chaînes de conversion de l'énergie


## Séance 6 Applications : estimation du rendement (cas importants à connaître)


### 6.1  Analyses documentaires par groupes, puis restitution collective.

- bilan énergétique d'une éolienne : notion de rendement

- bilan énergétique d'un alternateur : rendement et comparaison avec le précédent

- le diagramme de performance énergétique

- (en fonction du nombre de groupes) : 4ème sujet sur l'étude d'un banc de panneaux solaires


[Document support (en fonction du sujet) ici : ](https://framagit.org/ericb/documents/-/tree/master/Physique/Enseignement_Scientifique_Terminale/Theme2_le_futur_des_energies/Documents_utiles)


### 6.2   Restitution collective 

- Pour chaque groupe, établissement du diagramme de Sankey + prise de notes pour les autres groupes

- Point collectif sur les ordres de grandeurs à connaître pour chaque cas étudié.

- Complément : comparaison avec l'énergie thermique perdue annuellement par l'industrie


**SI LE TEMPS LE PERMET, ET EN FONCTION DE LA CLASSE CONCERNÉE** :

### 6.3   Transfert maximal en puissance

Remarque : une puissance correspond à un débit d'énergie.

- Cas du transfert maximum en puissance sur une charge résistive


## Séance 7


Évaluation commune : durée 1h


# Liens utiles. Sitographie


- 1  **[Modélisation de l'effet de serre par Jean-Louis Dufresne](https://www.ac-paris.fr/portail/jcms/p2_1982654/modelisation-de-l-effet-de-serre-par-jean-louis-dufresne)**

- 2  [Rapport du Haut-Conseil pour le climat](https://www.ac-paris.fr/portail/jcms/p2_2404487/rapport-annuel-2021-du-haut-conseil-pour-le-climat)

- 3  [Fusion nucléaire : on y est presque !](https://www.bbc.com/news/science-environment-60312633)

- 4  [Joint European Torus (JET)](https://ccfe.ukaea.uk/research/joint-european-torus/)

- 5  **[eProfs.fr : site proposant des vidéos et des fiches méthodologiques en sciences-physiques](https://eprofs.fr/)**

- 6  [Shift your job : choisir un travail pour contribuer à  la transition carbone](https://shiftyourjob.org/)

- 7  [Site QUE CHOISIR : l'audit énergétique obligatoire](https://www.quechoisir.org/actualite-audit-energetique-bientot-obligatoire-pour-la-vente-des-maisons-classees-f-ou-g-n94612/)

- 8  [Réseau du Transport de l'Énergie (haute tension seulement)](https://fr.wikipedia.org/wiki/RTE_(entreprise))

- 9  [Agence Internationale de l'Énergie](https://fr.wikipedia.org/wiki/Agence_internationale_de_l%27%C3%A9nergie)

- 10 [ENEDIS](https://fr.wikipedia.org/wiki/Enedis)

- 11 [Futurs énergétiques 2050 (RTE) ](https://www.rte-france.com/actualites/futurs-energetiques-neutralite-carbone-2050-principaux-enseignements)

- 12 **[Le site officiel énergie environnement point CH](https://www.energie-environnement.ch/maison/transports-et-mobilite/rechauffement-climatique)**

- 13 [Open data réseau énergies](https://opendata.reseaux-energies.fr/pages/accueil/)

- 14 [La clé des champs (magnétiqus)](https://www.clefdeschamps.info/carte-de-mesures/)

- 15 [DIMO diagnostique énergétique](https://www.dimo-diagnostic.net/actualite-diagnostic-immobilier/classement-energetique-maison)

- 16 **[Immeuble de poupées (conte de Pierre-André Magnin)](https://www.energie-environnement.ch/fichiers/contes/conte_immeuble_poupees.pdf)**

- 17 **[Le bocal de M. Redfish (conte de Pierre-André Magnin)](https://www.energie-environnement.ch/fichiers/contes/conte_redfish.pdf)**

- 18 [Mission S-Beauty (conte de Pierre-André Magnin)](https://www.energie-environnement.ch/fichiers/contes/conte_argor.pdf)

- 19 **[The carbon map (animation en temps réel)](https://www.carbonmap.org/#)**

- 20 **[Bilan radiatif de la Terre (Vincent Daniel)](https://planet-terre.ens-lyon.fr/ressource/bilan-radiatif-terre3.xml)**

- 21 **[Connaissance des énergies dot org](https://www.connaissancedesenergies.org/)**

- 22 **[Le Newton mètre](https://fr.wikipedia.org/wiki/Newton_m%C3%A8tre)**

- 23 **[Cours Mines-ParisTech 2019 de M. Jancovici](https://jancovici.com/publications-et-co/cours-mines-paristech-2019/cours-mines-paris-tech-juin-2019/)**

