Thème énergie

Séance 1

## Séance 1   Les énergies

### 1.1 Rappels de ce qui a été fait précédemment :

- une histoire de l'atmosphère terrestre
- le mécanisme de l'effet de serre

**[Lien vers la synthèse du thème 3](https://framagit.org/ericb/documents/-/blob/master/Physique/Enseignement_Scientifique_Terminale/Theme1_Oxydoreduction_du_fer/ressources/synthese_2021_12_20.pdf)**

**[Lien direct de la vidéo de Jean-Louis Dufresne (numéroté 1)](https://www.ac-paris.fr/portail/jcms/p2_1982654/modelisation-de-l-effet-de-serre-par-jean-louis-dufresne)**


### 1.2 Introduction : [un immeuble de poupées (cf le conte de Pierre André Mangin)](https://www.energie-environnement.ch/fichiers/contes/conte_immeuble_poupees.pdf)

- Que faut-il retenir de ce conte ?

### 1.3   Une petite histoire de la maîtrise de l'énergie et de ses conversions


**[Lien direct vers le contenu de la présentation](https://framagit.org/ericb/documents/-/blob/master/Physique/Enseignement_Scientifique_Terminale/Theme2_le_futur_des_energies/Seance1/Enseignement_Scientifique_Terminale_Theme_2_Energies.pdf)**


Cette partie a été inspirée par le site d'EdF : **[Les grandes dates de l'électricité](https://www.edf.fr/groupe-edf/espaces-dedies/l-energie-de-a-a-z/les-grandes-dates-de-l-electricite)**


Points importants :

- l'histoire des conversions de l'énergie
- ce qu'on a appris à faire avec l'énergie au cours du temps


