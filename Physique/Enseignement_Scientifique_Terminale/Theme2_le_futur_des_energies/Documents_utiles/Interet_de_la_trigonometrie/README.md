# Interêts de la trigonométrie pour l'ingénieur

Liste non exhaustive décrivant l'utilité de maitriser la trigonométrie et ses applications en terminale.


- 1.  formule d'Al Kashi
 
- 2.  Étude du prisme + aberrations de sphéricité + traversée d'une lame mince

- 3.  calcul de sin (x+h) et de cos (x+h)

- 4.  dérivée d'un vecteur

- 5.  valeur moyennes et efficaces en sinusoïdal

- 6.  battements

- 7.  séries de Fourier

- 8.  interférences

- 9.  formule des réseaux

- 10. loi de Bragg

- 11. produit scalaire : moment d'une force

- 12. notion de puissance réactive

- 13. facteur de puissance (EdF et les factures)

- 14. théorème de Leblanc (sens du chamo tournant)

- 15. Théorème de Ferraris (sens du champ tournant en triphasé => généralisation en n-phasé)

- 16. propriétés de parité de sinus et cosinus

- 17. études de battements d'un diapason (somme -> produit)

- 18. modulation d'amplitude

- 19. détection synchrone

- 20. linéarisation des intégrales

- 21. équation d'une ellipse en coordonnées polaires

- 22. mouvement élémentaire en coordonnées sphériques

- 23. angles d'Euler

- 24. ondes stationnaires // problème des conditions aux limites)

- 25. puissance absorbée (et transformée) par un dipôle

- 26. tension délivrée par un alternateur (somme de vecteurs déphasés du même angle + filtrage en peigne)

- 27. phénomène de diffraction à l'infini (étude de sinx / x = sinus cardinal + ses propriétés)

- 28. choix d'un repère de projection (élimination de la réaction du sol dans un problème

- 29. conditions de Gauss

- 30. filtrage en peigne

- 31. quantité conjuguée en complexe

- 32. multiplication de fréquence (non linéarités => pédale de distorsion)

- 33. conformateur à diodes

- 34. redressement polyphasé

- 35. ondes stationnaires

- 36. périodicité spatiale et temporelle

- 37. exponentielle complexe et ses propriétés

- 38. argument d'un réel

- 39. argument d'un rapport de deux complexes

- 40. argument de la somme

- 41. transformation d'un mouvement de rotation en un mouvement de translation (apparition de l'harmonique 2)

- 42. intérêt du triphasé (du n-phasé ensuite), filtrage d'harmoniques

- 43. operateur a = exp (j 2 pi/3) et ses propriétés + notion d'impédance cyclique

- 44. courbes de Lissajous

- 45. propriétés des ellipses

- 46. multiplication de fréquence (laser + doublage + triplage de fréquence)

- 47. vibrations (mécaniques et électriques) 



