# Thèmes des sujets posés aux examens médians en MQ83 (par année)

### P2021

- [MQ83 : Valeurs moyennes /Séries de Fourier (signal triangulaire) / Oscillations d'un objet avec frottements fluides](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2021/MQ83_P21_examen_partiel_sujet_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2021/MQ83_P21_examen_partiel_corrige_ericb.pdf)

### P2019

- [MQ83 : Effet Doppler + Analyse expérimentale des vibrations d'un verre (d'après Centrale Supélec 2018 TSI partie 1)](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2019/MQ83_P2019_examen_median_enonce_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2019/MQ83_P2019_examen_median_corrige_ericb.pdf)

### P2018

- [MQ83 : Notion de spectre de Fourier + Le Millenium Bridge (d'après Mines-Ponts 2016 PSI, partie 1 seulement) + Cône de Mach](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2018/MQ83_median_P2018_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2018/MQ83_median_P2018_ericb_corrige.pdf)

### P2017

- [MQ83 : Cours + Propagation d'ondes le long d'une corde tendue](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2017/MQ83_P2017_median_ericb_V4.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2017/MQ83_P2017_median_corrige_ericb_V6_avec_licence.pdf)

### P2016

- [MQ83 : Analyse de Fourier d'une impulsion + Extensomètre à corde vibrante](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2016/MQ83_P2016_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2016/MQ83_P2016_median_corrige_ericb.pdf)

### P2015

- [MQ83 : Cuve à ondes + Corde de Melde + Création de vagues](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2015/MQ83_P2015_examen_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2015/MQ83_P2015_examen_corrige_ericb.pdf)

