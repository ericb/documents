Examen partiel MQ83 / P2021 

Le sujet sera déposé ici même le mercredi 14 avril à 14h exactement (et envoyé par mail en double).

**L'épreuve commence à 14h**

**La durée de composition est de 1h30** + 45 min MAXIMUM afin que chaque étudiant puisse numériser et envoyer sa copie à l'adresse proposée.

**FORMAT OBLIGATOIRE POUR LE RENDU : Nom_Prenom_MQ83.pdf** ( vous pouvez -par exemple- utiliser le site https://www.ilovepdf.com/ )

TAILLE MAXI :  10 Mo 

Adresse d'envoi :  mon adresse mail eric.bachard  à l'utbm (je vous l'enverrai pendant l'épreuve, via moodle pour éviter les problèmes)


**COPIE ENVOYÉE APRÈS 16h15 le mercredi 14 avril 2021 : la note de 0/20 est automatiquement attribuée, sans recours possible**. Il n'y aura PAS  de trattrapage.


Correction : chaque copie sera annotée. Les notes seront rendues aussi rapidement que possible.


Eric Bachard
Le jeudi 8 avril 2021