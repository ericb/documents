/* Eric Bachard  2022 11 20 : mesure de pression avec le capteur MPX4250AP */

/* Ce programme réalise la mesure, la conversion et l'affichage de la tension en sortie du capteur MPX4250AP */

// la tension lue à la sortie du capteur est vue en entrée par le micro-contrôleur
const int AnalogInPin = A0 ; 

// variables utilisées pour la conversion ou l'affichage final
float pression ;
float tension ;

// initialisation
void setup()
{
    Serial.begin(9600);
}


// cette boucle se répète indéfiniment.
// Il faut donc bien prévoir ce qui va se passer : à chaque tour de boucle, on lit la tension à la sortie du capteur

void loop()
{

  // ACQUISITION : la valeur lue, fonction de la pression, est convertie en V sur une plage 
  // pouvant varier de 0V à 5V ; cette valeur lue étant comprise entre 0 et 1023
  tension = analogRead(AnalogInPin) * 5.0 / 1023.0; 

  // Calcul de la pression en hPa
  
  /* 
   * D'après la documentation, la valeur de la tension lue est telle que
   * tension = 5,0 x (pression . 0,004 - 0,04), avec la tension exprimée en kPa
   * soit tension = (pression . 0,02) - 0,2 
   * en inversant, il vient : pression = 10.0 + tension/0.02  en kPa
   * on multiplie toutes les valeurs par 10 pour avoir le résultat en hPa 
   * Soit : pression = 100.0 + tension / 0.002;
   */
  
  pression = 100.0 + tension/0.002 ; // pression en hPa

  // Affichage de la pression sur le moniteur série (variante : utiliser l'afficheur, cf prochain programme)
  Serial.print(pression, 0);
  Serial.println(" hPa");

  // on attend 1s avant de recommencer, en redonnant la main au système pour exécuter
  // les autres programmes en cours. Si on n'utilisait pas cette boucle le processeur
  // serait utilisé à 100% pour ce programme, et l'environnement paraitrait très lent (lag)
  delay(1000);
}


