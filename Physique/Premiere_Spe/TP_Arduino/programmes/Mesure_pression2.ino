
// Pour mémoire  https://www.arduino.cc/reference/en/libraries/

// inclusion de la bibliothèque associée à l'afficheur LCD
#include <Wire.h>
#include <LiquidCrystal.h>

// la tension lue à la sortie du capteur est vue en entrée par le micro-contrôleur
const int AnalogInPin = A0 ; 

// int : signifie entier et float signifie décimal
int val;
float tension;
float pression;

//creation de l'instance lcd d'un objet de type afficheur (constructeur)
LiquidCrystal  lcd(12,11,5,4,3,2);


void setup()
{
  //configure l'afficheur 16 lig 2 col
  lcd.begin(16, 2);

  // affichage première ligne
  lcd.print("Tension :");
}

void loop()
{

  // ACQUISITION : la valeur lue, fonction de la pression, est convertie en V sur une plage 
  // pouvant varier de 0V à 5V ; cette valeur lue étant comprise entre 0 et 1023
  tension = analogRead(AnalogInPin) * 5.0 / 1023.0; 

  // Calcul de la pression en hPa
  
  /* 
   * D'après la documentation, la valeur de la tension lue est telle que
   * tension = 5,0 x (pression . 0,004 - 0,04), avec la tension exprimée en kPa
   * soit tension = (pression . 0,02) - 0,2 
   * en inversant, il vient : pression = 10.0 + tension/0.02  en kPa
   * on multiplie toutes les valeurs par 10 pour avoir le résultat en hPa 
   * Soit : pression = 100.0 + tension / 0.002;
   */
  
  pression = 100.0 + tension/0.002 ; // pression en hPa

  // Affichage de la pression sur l'afficheur
  // placement curseur debut 2eme ligne
  lcd.setCursor(0, 1);

  //affichage de la tension
  lcd.print(pression);
  lcd.print(" hPa "); 

  // on attend 1s avant de recommencer, en redonnant la main au système pour exécuter
  // les autres programmes en cours. Si on n'utilisait pas cette boucle le processeur
  // serait utilisé à 100% pour ce programme, et l'environnement paraitrait très lent (lag)
  delay(1000);
}
