#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

# Noter les différentes valeurs de volume séparées par une virgule
x=np.array([50,45,40,35,30,25,20])

# Relever, pour chaque volume de la seringue, les différentes valeurs de pressions mesurées
# et remplacer les valeurs de ce tableau (qui sont fausses)
y=np.array([800,850,900,950,1000,1050,1100])

fig=plt.figure(1,figsize=(10,10))

ax1=plt.subplot2grid((18,18),(0,0), rowspan=7, colspan=8)
ax1.set_title('P = f(V)', color ='blue', fontsize=18)
ax1.set_xlabel('Volume (en mL)', color='grey')
ax1.set_ylabel('Pression (en hPa)', color='grey')

plt.plot(x,y,'or:')
#plt.show()


ax2=plt.subplot2grid((18,18),(0,10), rowspan=7, colspan=8)
ax2.set_title('V = f(P)', color ='blue', fontsize=18)
ax2.set_xlabel('Pression (en hPa)', color='grey')
ax2.set_ylabel('Volume (en mL)', color='grey')

plt.plot(y,x,'or:')
#plt.show()

ax3=plt.subplot2grid((18,18),(10,5), rowspan=7, colspan=8)
ax3.set_title('PxV = f(V)', color ='blue', fontsize=18)
ax3.set_ylabel('P.V (unité à trouver pour 1pt)', color='grey')
ax3.set_xlabel('Volume (en mL)', color='grey')
ax3.set_ylim(0,60000)
plt.plot(x,x*y,'or:')

plt.show()
