# Activités expérimentales prévues en BTS2 CIEL (année 2024-2025)

## Matières concernées : 
- physique
- co-enseignement physique et informatique


## Informations complémentaires

Les textes seront ajoutés progressivement. Certains TP seront faits en rotation car il n'y a pas assez de matériel, mais tous les documents utiles à la mise en oeuvre seront disponibles en ligne.

Les logiciels utilisés sont pour la plupart libres (licences MIT, Apache ou (L)GPL), sinon fournis par les fabricants de matériel (comme [SALEAE](https://www.saleae/com) par exemple, avec le logiciel **Logic 2**).


Merci d'avance pour tout retour constructif.


© Eric Bachard / 12 août 2024
Tous droits réservés
