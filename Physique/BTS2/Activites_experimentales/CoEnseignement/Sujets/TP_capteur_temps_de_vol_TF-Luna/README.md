[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)

# TP mesure de distance à l'aide d'un capteur "temps de vol" tye TF-Luna

![Le module TF-Luna](./ressources/module_TF-Luna.png)


## Matériel utilisé


- un capteur TF-Luna (cf [ressources](./ressources)) ;
- OScilloscope avec sonde et décodeur I2C intégrés ou analyseur logique type SALEAE avec logiciel PulseView ou Logic;
- Arduino UNO ou équivalent ;
- fils et plaque de montage ;
- un banc d'optique ;
- un écran blanc ;
- ordinateur portable

## Préparation du TP

:memo: Les réponses aux questions ci-dessous sont à rédiger dans la préparation du TP.


    Q1 En vous aidant de la documentation (cf [ressources](./ressources)), retrouver le brochage du capteur TF-Luna

    Q2 Quelles sont les distances nominales pouvant être mesurées ? 

    Q3 Rappeler le principe de la mesure de distance avec ce capteur.

    Q4 Application numérique : en supposant qu'on fasse la mesure d'un objet distant de 5m. Combien dure la mesure ?

    Q5 Quelle est la résolution de ce capteur ? Donner l'ordre de grandeur de la précision que l'on peut en attendre

    Q6 Quelles mesures faut-il prendre au niveau du braocahge si on veut utiliser ce capteur avec le bus I2C ?

    Q7 En considérant que l'on utilise ce capteur avec une batterie de capacité 2000 mAh, quelle sera l'autonomie ?

    Q8 Même question si on veut l'utiliser avec le bus série. Est-ce compatible RS232 ?

    Q9 Donner le schéma du montage pour réaliser une mesure de distance avec le banc d'optique



:warning: **Cette préparation devra être rendue en début de TP**


## Partie 1 Montage et Mesures

![Brochage du module TF-Luna (cf documentation récente](./ressources/brochage_TF-Luna.png)


Faire un schéma du montage permettant d'estimer la précision de ce capteur, en utilisant le banc d'optique.

Réaliser le montage correspondant et appeler le professeur avant de mettre en route le dispositif. 


## Partie 2 : enregistrement des échanges sur le bus I2C

Proposer une méthode permettant d'enregistrer les échanges sur le bus I2C.

Régler le déclenchement pour que le début de la transaction apparaisse. Repérer les starts et les stops (il faut justifier)

N.B. : il pourra être utile de faire une sauvegarde de l'enregistrement.


## Partie 3 : 

