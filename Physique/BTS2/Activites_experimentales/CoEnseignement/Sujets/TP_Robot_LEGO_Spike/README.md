[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)

# Présentation du TP

![Base motrice 3](./ressources/base_motrice3.png)


Le matériel mis à disposition est pour l'instant un matériel personnel (pour des raisons de délai d'approvisionnement)

**Il est impératif de faire toute manipulation avec le plus grand soin et la plus grande rigueur possibles. En cas de doute DEMANDER au professeur !**



## Objectifs du TP

- [x] Construire la maquette attendue (elle sera utilisée tout le TP)
- [x] S'approprier l'API LEGO officielle (classes, quelques méthodes à tester)
- [x] Faire fonctionner le robot LEGO Spike en testant quelques algorithmes avec l'API LEGO officielle
- [x] Mettre en oeuvre quelques capteurs (gyroscope, couleurs, force, moteurs), et valider leurs fonctionnements avec le robot
- [x] Réaliser un algorithme de rotation (1/4 de tour ou demi tour) ou de déplacement sur une distance donnée
- [x] Passer à la seconde API (micropython) et prendre le contrôle du hub via une connexion USB
- [x] Réaliser une fiche recette pour valider un algorithme testé (au choix)

Le compte rendu devra être rendu avant la fin de la séance.

## Liste du Matériel

- coffret 45678 LEGO Spike principal (incluant le hub, le câble USB, les capteurs et toutes pièces utiles)
- coffret 45681 LEGO Spike complémentaire (incluant le jeu de capteurs supplémentaires, des roues spécifiques et toutes les pièces utiles à la réalisation de la maquette prévue


Les 2 boitiers étant neufs et complets, une vérification en début et en fin de TP sera effectuée.



# Présentation du hub LEGO Spike. Utilisation de l'API officielle

:warning:  **Le navigateur chromium DOIT obligatoirement être utilisé pour ce TP (ungoogled chromium est encore mieux), car Firefox ne convient pas.**

:warning:  **Si vous utilisez noscript, il faut autoriser javascript pour les sites legoeducation.com et lego.com, sinon rien ne fonctionnera**


## Travail demandé avec l'API LEGO

Réaliser la maquette et tester chaque capteur

Pour commencer, se connecter sur le site : https://spike.legoeducation.com/prime/models/ et suivre les instructions.

Il est demandé de faire fonctionner plusieurs programmes parmi ceux proposés.

Ensuite reprendre le code proposé (demander à l'enseignant).

(à suivre)

## TODO : complete me


tester les différents programmes


# Présentation de l'API Spikedev


:memo: Le Hub LEGO Spike contient un microcontrôleur, lui même piloté par un système d'exploitation (OS).
Il est possible d'y lancer un shell par l'intermédiaire de screen, et d'y installer des classes python,
et de les utiliser comme d'autres classes python. Spikedev est une bibliothèque micropython qui sera copiée
sur le hub, dans un dossier spikedev. **L'API de spikedev est décrite** [**À CETTE ADRESSE**](https://dwalton76.github.io/spikedev/spikedev-button.html).

Les étapes qui suivent permettent de réaliser toutes ces tâches sans problème particulier, toutefois, **pour
la suite, on considérera que spikedev est déjà installé dans le hub.**

:memo: en cas de problème grave, il est possible de reflasher le hub sur le site de Lego (en utilisant Chromium).


# INSTALLATION de spikedev


N.B. : **on suppose que pip3 est installé sur la machine**, ainsi que Python 3.10+ (conseillé).

Le bluetooth n'étant pas fiable on va utiliser l'USB pour piloter le hub et programmer le robot et faire quelques essais.

:memo: **on supposera que le périphérique branché en USB sera accessible via le port série /dev/ttyACM0,
et l'utilisateur devra appartenir au groupe dialout (ls -l /dev/ttyACM0)** (sinon utiliser sudo)


## Première étape : télécharger spykedev (en utilisant git)

Avant toute chose, il faut installer **ampy (de AdaFruit)**. Pour l'utilisateur local, il suffit d'installer ampy sans sudo.

```
pip3 install adafruit-ampy  // attention ampy n'est alors installé QUE pour l'utilisateur (sinon faire : pip3 install adafruit-ampy)

```

ampy est l'outil (appelé par la méthode install ci-dessous), qui servira à copier les classes (des fichiers .py dans le module spike)


```
$ git clone https://github.com/dwalton76/spikedev.git
$ cd spikedev
$ sudo python3 ./utils/spike-install-spikedev.py
```

## Exécuter un programme depuis le desktop

### Étape 1 : écrire le programme

Des petits programmes de démonstration sont proposés dans le dépôt de spikedev. Il est assez facile de les tester.


### Étape 2 : tester le programme

```
ampy --port /dev/ttyACM0 run demo/hello-world/hello-world.py

```

### Exécuter le code depuis le hub, en étant connecté

```
screen /dev/ttyACM0   

 # (éventuellement CTRL+C si l'écran semble bloqué) 
MicroPython v1.20.0-544-g977dc9a36 on 2023-11-22; SPIKE Prime with STM32F413
Type "help()" for more information.
>>> help()
Welcome to MicroPython!

For online docs please visit http://docs.micropython.org/

The hub can be configured through: hub.config[key]
E.g. hub.config["hub_name"] = "Spike"

The following keys are available:
hub_name                  (str)  -- set the hub name
usb_force_power_on      * (bool) -- power on hub when usb is connected

hub_os_enable           * (bool) -- enable hub os or boot to repl
hub_os_handle_bluetooth * (bool) -- disable to allow "import bluetooth"
hub_os_shutdown_timeout * (int)  -- shutdown timeout in seconds
hub_os_auto_advertise   * (bool) -- auto advertise on ble

* = will reset to default on soft reset - place in boot.py to persist.

Control commands:
  CTRL-A        -- on a blank line, enter raw REPL mode
  CTRL-B        -- on a blank line, enter normal REPL mode
  CTRL-C        -- interrupt a running program
  CTRL-D        -- on a blank line, do a soft reset of the board
  CTRL-E        -- on a blank line, enter paste mode

For further help on a specific object, type help(obj)
For a list of available modules, type help('modules')
>>> help()
>>> import hub
>>> hub.
battery_current                 battery_temperature
battery_voltage                 bootloader      button
config          device_uuid     hardware_id     light
light_matrix    motion_sensor   port            power_off
reset           soft_reset      sound           temperature
usb_charge_current
>>>
>>> 
>>> hub.battery_current()
66
>>> hub.battery_voltage()
8249

```

### Comment quitter screen ?

screen lance une session en remote.

On peut soit quitter cette session (et y retourner), soit quitter et terminer screen.

#### Pour quitter la session et se détacher (pour éventuellement se reconnecter plus tard)

CTRL + A  suivi de CTRL + D


Pour se reconnecter, il suffit de taper la commande :

```
screen -r  # dans le MÊME terminal, évidemment ;-)
```

**Vérification** :

```
me@my_machine ~$ screen /dev/ttyACM0 
[detached from 168035.pts-3.be]
me@my_machine:~$ screen -r
[screen is terminating]
me@my_machine:~$ 
```

#### Pour quitter la session et la terminer

CTRL+A suivi de l'appui sur ":" (sans relacher CTRL +A)

=> dans le bas, apparait l'invite ":" qui correspond au mode commande de emacs.

quit (suivi de return) permet de terminer la session screen.


Dans le terminal, c'est confirmé par :


```
$ screen /dev/ttyACM0    # ouverture de la session screen
[screen is terminating]  # fin de la session
```

####


### Exemples pour comprendre comment utiliser l'API spikedev


```
>>> import spikedev
>>> import spikedev.motor
>>> spikedev.motor.
Motor           utime           log_msg         MAXINT
MININT          BUSY_MODE       BUSY_SENSOR     BUSY_MOTOR
MotorSpeed      MotorSpeedPercent               MotorSpeedRPS
MotorSpeedRPM   MotorSpeedDPS   MotorSpeedDPM   MotorStop
MotorCallbackEvent              MotorPolarity   MotorMode
InvalidMotorMode                _portletter2motor
_callback       _callback_A     _callback_B     _callback_C
_callback_D     _callback_E     _callback_F     SpikeMediumMotor
SpikeLargeMotor
>>> spikedev.motor.
```

:memo: **Pour éteindre le robot** :

```
>>> hub.power_off()
me@my_machine:~$ screen -r
[screen is terminating]
```

## Programmes réels (avec le câble USB débranché ou la connexion bluetooth inactive)


Plusieurs choix sont possibles :

### Utilisation de l'API Officielle

Le plus simple, consiste à utiliser chromium (Ungoogled Chromium est vivement conseillé) en se connectant sur site de LEGO, à l'URL :

[https://spike.legoeducation.com/prime/models/](https://spike.legoeducation.com/prime/models/)

- cliquer sur "nouveau projet" et suivre les instructions
- lors de l'écriture du code, ne pas oublier de choisir un numéro au programme, qui sera "téléversé" dans le hub (façon "Arduino")
- noter le numéro du programme ;
- une fois le programme testé et fonctionnel, il est possible de l'exécuter directement, en le sélectionnant sur le hub (qui doit être allumé)
- un appui sur le bouton central lancera l'exécution de ce programme, en toute autonomie.

### Utilisation de ampy

De la même façon qu'on a installé ampi et utilisé cette application pour installer spikedev dans le hub, on peut "téléverser" un programme dans le hub et l'exécuter après avoir choisi le bon numéro de programme avec les boutons.

Les utilitaires disponibles sont :

- **spike-copy-filesystem.py**
- **spike-get-file.py**
- **spike-run-main.py**
- **spike-exit-main.py**
- **spike-install-spikedev.py // déjà utilisé pour installer spikedev sur le hub**
- **spike-slots.py**
- **spike-get-file-in-slot.py**
- **spike-push-file-to-slot.py**
- **spike-stop-motors.py**

La commande à passer est : `$ sudo ampy --port /dev/ttyACM0 run COMMANDE`  (voir liste ci-dessus pour la commande et surtout son argument)

Toutes les explications sont dans le README du projet spikedev. Voir [**le site de spikedev**](https://dwalton76.github.io/spikedev/)

TODO (complete me)
