[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)

# TP Capteur à temps de vol VL53LOX laser

![**Capteur VL53L0X (non assemblé)**](./ressources/capteur_VL53LOX.png)

**But de ce TP** : étudier le capteur de distance par temps de vol (Time of Flight) VL53LOX, et réaliser une fiche recette permettant de le réutiliser.


## Préparation

:warning: **TOUTES ces questions sont à préparer AVANT le TP**


### 1 Approche théorique et exploitation de la documentation technique

Q1

Q2

Q3

Q4

Q5

Q6

Q7

Q8

Q9

### 2 Proposer un mode opératoire (schémas, liste du matériel), pour toutes les mesures prévues.


## Partie 1  Réalisation du montage

- réaliser le montage et le faire vérifier;
- visualiser les échanges sur le bus I2C (oscilloscope ou analyseur logique) ;
- retrouver l'adresse du capteur sur le bus, et identifier les valeurs mesurées ;

Matériel utilisé :

- circuit imprimé avec capteur VL53L0X
- 1 arduino UNO + cable USB A+B (USB2)
- fils de connexion ;
- plaque pour montages ;
- oscilloscope capable de visualiser le bus I2C ou un analyseur logique ;
- disque rotatif avec mesure agulaire possible ;
- banc d'optique ;
- cavaliers ;
- écran blanc.


## Partie 2 Mesures

- tracer la caractéristique valeur mesurée / distance sur le banc d'optique ;
- discuter la pertinence de la méthode ;
- en tenant compte du matériel disponible, proposer une méthod permettant de déterminer le secteur angulaire dans lequel la mesure est possible ;


## Partie 3 : exploitation des mesures

- estimer le domaine où la mesure de distance est précise, et celui au delà duquel elle ne l'est plus ;
- vérifier les conditions de sécurité de cette manipulation.

## PArtie 4 : synthèse du TP

**Rédiger le compte rendu, qui sera à rendre AVANT la fin du TP, et par email.**



    © Eric Bachard  21 août 2024. Tous droits réservés.