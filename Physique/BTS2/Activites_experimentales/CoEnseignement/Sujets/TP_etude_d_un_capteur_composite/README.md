
[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)

# Thème : étude d'un capteur "composite" + transmission série d'informations

Avant propos : on ne parle pas de capteur intelligent ici, mais d'un capteur composite, parce que son fonctionnement est régi par un microcontrôleur (ce qui sous entend une grande complexité), et non pas parce qu'une IA est utilisée. Toutefois, cette utilisation n'est absolument pas exclue (en effet, rien n'empêche
de collecter les mesures pour entraîner une IA).

![Aspect du capteur DHT11 vu de face](./mesure_DHT11/DHT11_face_avant.png)
*Aspect du capteur composite DHT11 (vu de face)*

 
Documents fournis ( dans le dossier **[ressources](./ressources)**) :

- une copie du TP proposée par la marque Elegoo (Lesson 11, incluant certains schémas utiles) ;
- une analyse du protocole utilisé (exemple de mesure d'un taux d'humidité) ;
- la bibliothèque à installer (sur sa machine personnelle) ;
- un programme exemple pour Arduino.

Matériel fourni : 

- un Arduino UNO R3 Elegoo (ou équivalent) ;
- un capteur composite DHT11 ;
- câbles utiles à la manipulation ;
- un oscilloscope numérique.

Pour réutiliser les chronogrammes, penser à amener une clé USB. Elle sera obligatoirement passée en station blanche juste avant le TP.

## Partie 1 : préparation du TP

### BUTS de cette partie

* lire les documents fournis (en ligne)
* extraire des informations de la documentation et répondre aux questions ;
* réaliser la liste du matériel permettant de faire fonctionner le montage ;

**Préparation** (à rendre au début du TP) :

**Q1** Combien de fils sont nécessaires au transport des données avec le composant DHT11 ?

**Q2** Comment les données seront-elles recueillies avec l'Arduino ? Quelle est la nature (analogique/numérique ?) de ces données ?

**Q3** Quelles valeurs peuvent prendre le taux d'humidité et la température ?

**Q4** Avec quelle précision les mesures sont-elles faites ? (il faut justifier la réponse)

**Q5** À quel type de bus ressemble ce bus, et pourquoi est-ce toutefois différent dans ce cas ?

**Q6** En vous aidant de la documentation, expliquer et dessiner un signal représentant le start, en précisant QUI impose QUOI.

**Q7** En vous aidant de la documentation, expliquer et dessiner un signal représentant un 1 

**Q8** Même question pour expliquer le zéro.

**Q9** PRÉVOIR une liste des manipulations à faire pendant ce TP, et proposer une liste PRÉCISE du matériel à utiliser, ainsi que le mode opératoire, pour chaque mesure


**IMPORTANT** :  Les manipulations prévues, la liste du matériel et le mode opératoire devront figurer dans la préparation.

## Partie 2

### Buts de cette partie

- installer la bibliothèque DHT11 (fournie) dans votre bibliothèque Arduino ;
- réaliser le montage utilisant le DHT11 ;
- écrire le code, le compiler et le tester avec l'Arduino fourni ;
- réaliser un enregistrement en mode monocoup (single) ;
- vérifier que la valeur de % d'humidité et de température sont conformes à celles attendues (la justification des résultats est obligatoire)
- montrer que l'Arduino peut fonctionner de façon autonome, et le faire constater.

N.B. : penser à choisir un oscilloscope qui permet d'enregistrer des copies d'écran sur clé USB


### Simulation du composant DHT11 à l'aide de SimulIDE

Le logiciel libre [**SimulIDE**](https://simulide.com/p/downloads/) permet de simuler l'association d'un Arduino avec un capteur composite DHT22. 

La simulation est fournie. Il faut télécharger le fichier [**dh22_test.sim1**](./ressources/SimulIDE_Arduino/Arduino_DHT22/dht22_test.sim1). Une fois téléchargé placer le fichier dans un repértoire avec les droits en écriture.

Ensuite, lancer SimulIDE (demander au professeur pour l'installation de la version la plus récente du logiciel). Ensuite, ouvrir la simulation dans l'IDE. Pour la suite, il suffit de **redéfinir le composant comme étant un DHT11** (il est normalement reconnu) et commencer la simulation.

Noter les points positifs, et les éventuels soucis (toute remarque constructive est la bienvenue).


**Travail demandé :**

- tester la simulation et vérifier que le comportement est très proche de celui constaté avec le vrai composant (revoir la documentation en cas de besoin)
- à l'aide du simulateur, ajouter un analyseur logique, relier SDA et SCL et lancer l'exécution (on pourra définir un trigger (passage de 1 à 0 de SDA par exemple)
- retrouver une valeur de pression ou de température, et vérifier la validité de la mesure.

:memo: il y a peut-être un bug avec la simulation, et il pourrait être intéressant de relier les 2 bornes du DHT11 qui ne sont pas reliée en début de simulation.

## Partie 3 (Partie pro)

### Fiche recette pour l'utilisation du DHT11

Proposer une fiche recette permettant de réutiliser ce capteur pour une simple mesure de température.

**N.B.: les schémas doivent être obligatoirement inclus**.


### À faire si le temps le permet

Réaliser un programme (C, C++ ou Python au choix) qui, à partir d'un mot de 40 bits (base 2), vérifie la parité cf le principe
étudié dans la partie 2.


## Synthèse

**Réaliser le compte rendu qui sera envoyé par mail AVANT la fin du TP.**

