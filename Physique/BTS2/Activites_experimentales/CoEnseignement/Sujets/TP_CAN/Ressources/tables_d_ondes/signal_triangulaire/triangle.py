#!/usr/bin/python

# © Eric Bachard    26 août 2024

# Création d’une table d’onde triangulaire en Python 3.9 pour un tableau de données en langage C 
# utilisant des octets non signés. Tableau à 256 entrées, plage d’amplitude 0-255

# Dernier changements : le nom contient la valeur du rapport cyclique, compris entre 0.1 et 0.9

import math

fn = input("Entrer le nom du fichier tableau à créer: ")
rc = input("Entrer le rapport cyclique sous la forme d'un nomnre décimal compris entre 0.1 et 0.9 : ")

fichier = open( fn+"_rc_"+rc+".h", "w+" )

step1 = (1./float(rc))/2.0
step2 = -(1./(1.0 - float(rc)))/2.0

fichier.write( "unsigned char triangle_table_uint8[255] =\n{" )

v = 0
step = step1

for x in range(255):

    # On saute une ligne toutes les 16 valeurs
    a_count = x % 16

    if a_count == 0:
        fichier.write("\n    ")

    if (x < 254):
        fichier.write( hex(int(v)) )
        if v < 16:
            fichier.write( " ," )
        else:
            fichier.write("," )
        if (x % 16 != 15):
            fichier.write(" ")
    else:
        fichier.write( hex(int(v)) + "\n};\n\n" )

    v += step

    if (v >= 127 ):
        v = 127
        step = step2

    if (v < 0):
        v = 0

fichier.close()


