#!/usr/bin/python

# Création d’une table d’onde en Python 3.3 pour un tableau de données en langage C utilisant des octets non signés
# Tableau à 256 entrées, plage d’amplitude 0-255
# Note : renommer le tableau en fonction de l’onde générée
# © Eric Bachard  26 août 2024

# inspiré de : https://laboratory-manual-arduino.developpez.com/tutoriels/manuel-laboratoire-controleurs-embarques/?page=generateur-de-formes-d-onde-arbitraires

import math

fn = input("Entrer le nom du fichier tableau à créer: ")
fichier = open( fn, "w+" )

fichier.write( "unsigned char sinus_table_uint8[255] =\n{" )

for x in range( 255 ):

    # On saute une ligne toutes les 16 valeurs
    a_count = x % 16

    if a_count == 0:
        fichier.write("\n    ")

    # Chaque donnée représente 1/256ème du cycle, la valeur de crête est 127
    f = 127.0 * math.sin( 6.28319 * x / 256.0 )
    v = f + 128

    # Empêche de sortir de l’intervalle valide au cas où
    if v > 255:
        v = 255

    if v < 0:
        v = 0

    if x < 254:
        # on convertit en entier à ce moment seulement (sinon erreurs cumulées)
        fichier.write( hex(int(v)) )
        if v < 16:
            fichier.write( " ," )
        else:
            fichier.write("," )

        if (x % 16 != 15):
            fichier.write(" ")
    else:
        fichier.write( hex(int(v)) + "\n};\n\n" )

fichier.close()


