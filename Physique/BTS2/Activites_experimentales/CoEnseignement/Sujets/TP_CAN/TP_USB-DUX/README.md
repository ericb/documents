[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)

# TP Conversion Analogique Numérique et Numérique Analogique à l'aide du boîtier "DAQ" USB-DUX-D (acquisition de données)

## Objectifs du TP

**Plusieurs parties sont à considérer dans ce TP (prévu pour 2 séances de 2h)** :
- (1)  le codage d'une grandeur analogique en hexadécimal pour au moins deux cas : signal sinusoïdal et signal triangulaire de rapport cyclique à définir ;
- (2)  le rangement de toutes les valeurs dans un tableau (dans un fichier .h, afin de pouvoir l'utiliser avec l'Arduino) ;
- (3)  l'écriture d'un programme permettant à l'Arduino d'envoyer les valeurs du tableau sur son port numérique (et par suite à l'entrée du DAQ);
- (4)  la compréhension du fonctionnement de l'USB-DUX ;
- (5)  l'écriture et la compilation de programmes permettant de faire fonctionner ce boitier d'acquisition (DAQ) ;
- (6)  l'écriture du schéma du montage et de la liste du matériel ;
- (7)  la mise en oeuvre de l'USB-DUX pour convertir ces données **numériques** en données analogique ;
- (8)  la mise en oeuvre du système complet (ordinateur portable + Arduino + DAQ + Oscilloscope + câbles et matériel nécessaire) ;
- (9)  l'utilisation d'un oscilloscope pour visualiser le signal **analogique** délivré en sortie de l'USB-DUX ;
- (10) l'exploitation des résultats de mesure et l'écriture du compte rendu.

Commentaires :
- Les parties 1 à 5 concernent la préparation du TP. En particulier la partie 1 a été faite en cours avec toute la classe (le classeur, au format .ods, est dans les ressources)
- Pour les parties 2 et 3, tout est donné dans les ressources. Les scripts proposés sont en python, et il faudra réaliser Deux fichiers qui seront utilisés par l'Arduino
- Pour les parties 4 et 5, toute la documentation est en ligne (cf liens proposés) et dans les ressources.
- Pour les partie 4 et 5 : celle-ci seront réalisées devant le professeur dans la première partie du TP. Le but étant de bien comprendre le fonctionnement du DAQ étudié, afin de bien le programmer.
- L'écriture du code permettant de faire fonctionner le DAQ **pour son utilisation précise dans le TP** sera réalisée entre les 2 séances (et donc servira de préparation à la seconde séance).
- Les parties 7 à 10 concernent la seconde séance.


**Compétences, Savoir-faire expérimental et savoir être professionnel attendus et évalués dans ce TP** :
- comprendre la chaîne de conversion du signal analogique -> numérique -> analogique suivie par le signal ;
- réaliser une véritable **conversion numérique-analogique** ;
- prévoir le matériel à utiliser, et gérer sa mise en place ;
- écrire du code en Python pour générer les tables d'ondes ;
- écrire du code en C++ pour faire fonctionner l'Arduino comme un générateur de signaux numériques ;
- écrire du code en C pour faire fcontionner le DAQ ;
- établir un protocole complet pour les mesures (schéma du montage + mode opératoire) ;
- étudier le fonctionnement du DAQ et réaliser quelques mesures avec cet appareil dont l'intérêt est surtout pédagogique ;
- valoriser les mesures obtenues et rendre compte du travail réalisé avec une approche professionnelle.

## Préparation du TP

### Génération d'un signal numérique.

#### Réalisation d'une sinusoïde

En cours, nous avons vu comment on échantillonne une fonction sinusoïdale. Si la fréquence d'échantillonnage est suffisante, c'est à dire si on collect suffisamment d'échantillons pendant une période, alors la reconstitution du signal sera fidèle au signal original (théorème de Shannon).

Pour cela, on supposera que l'on limite le spectre du signal échantillonné avec un filtre anti-repliement placé à l'entrée de la chaîne de conversion. En supposant cette condition réalisée, on va réaliser la conversion des grandeurs analogiques (comprises entre -1 et 1) de la représentation d'une sinusoïde en valeurs hexadécimales avec la convention suivante :
- le codage se fera sur 2 octets, soit entre 0x00 et 0xFF (on se limite à l'étude du principe dans ce TP) ;
- la valeur la plus négative de la sinusoïde (-1) sera encodée 0x00, et la valeur la plus grande (+1) sera encodée 0xFF ;
- la valeur 0 analogique sera encodée 0x80 (signal centré, de valeur moyenne nulle).

:warning: En cas de problème, dans les ressources, des exemples sont proposés dans le répertoire "tables d'ondes". Les script fonctionnent et ont été testés avec python3.8 et python3.10

### Le boîter d'acquisition (DAQ).

![CAN/CNA USBDUX-D, schéma interne](../Ressources/topview.png)


L'ensemble se présente comme une boîte avec plusieurs connections possibles via :

- un port USB2 (femelle)
- un port DB25 femelle (la fiche mâle avec les fils soudés est fournie dans le TP)
- un connecteur 15 broches (femelle, mais adaptateurs possibles)

On remarquera que le brochage est redonné dans l'image ci-dessous (en plus de la documentation proposée).

=>   [Le brochage est donné sur la face avant(cf ci-dessous), mais aussi dans ce document](../Ressources/usbdux_pinout.pdf)


![USBDUX-D, en vue de face](../Ressources/USBDUX-D_face.jpg)




###  Compréhension du fonctionnement du DAQ

- étudier et comprendre les exemples fournis par le constructeur de l'USBDUX-D

:memo: le code source à tester est dans l'archive du projet Comedi Record, qui est présente dans les ressources (on peut aussi clôner [**le dépôt git**](https://github.com/berndporr/comedirecord) )


### Conversion numérique analogique

(tester le code dents de scie avec l'Arduino)

- réaliser une table d'onde de la fonction sinus d'amplitude 1, sur 256 échantillons (le programme exemple est fourni)
- à l'aide d'un Arduino, appliquer cette table d'onde à l'entrée NUMÉRIQUE ( bien réfléchir aux bits utilisés) de l'USBDUX-D, et visualiser la sortie analogique (canal 0 ou 1 de l'USBDUX-D)

Pour vous aider, des exemples sont proposés [dans les ressources](../Ressources/tables_d_ondes).


(à placer dans la préparation du TP)

    Question 1 : décrire la chaîne de traitement de l'information dans ce cas précis. En particulier, a-t-on besoin d'unn échantillonneur ? D'un bloqueur ?

    Question 2 :  Expliquer la conversion réalisée. A-t-on besoin d'un filtre passe-bas ? Pourquoi ?

### Conversion analogique numérique


## Manipulations

### Partie 1 : compréhension du fonctionnement de l'USBDUX-D

Les programmes exemples sont à compiler depuis l'ordinateur sous Linux. Pour commencer, il faut brancher l'USBDUX à l'ordinateur, via le câble USB fourni.


### Partie 2 : utilisation de l'Arduino pour réaliser une conversion numérique analogique

Mise en avant des performances (et aussi des limites) de l'Arduino.

:warning: **complete me**

## Synthèse

:warning: **TODO**

:memo: **NOTE:** Une bonne partie des bibliothèques, fichiers sources etc, sont disponibles dans le dossier [**Ressources**](../Ressources)

:warning: **Attention:** pour faciliter la compilation, les sources de libiir1 et comedirecord sont fournies, mais ce n'est qu'un contournement, par exemple pour gagner du temps. Il est conseillé de télécharger les sources issues des dépôts github.

