[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)


# Utilisation d'un analyseur de réseau vectoriel d'entrée de gamme

Ce TP est un complément du TP sur les lignes (câble coaxial étudié en physique), mais appliqué aux antennes. Il sera un support pour le cours de 2ème année sur les antennes.


L'appareil de mesure utilisé dans ce TP est un **analyseur de réseau vectoriel** (car il peut calculer et afficher deux composantes d'une impédance en fonction de la fréquence). Cela permet de réaliser des courbes dans un plan (par exemple en utilisant l'Abaque de Smith). Celui que nous utiliserons est le VNA 1201SA de marque AAI (qui signifie Accuracy Agility Instrument). Il fonctionne dans le domaine des fréquences radio entre 0,34 MHz et 3 GHZ (fréquences dites "RF"), essentiellement selon 2 modes :

- **le mode point** : à une fréquence donnée, en affichant toutes les valeurs (fréquences, VSWR, S11, Z, X, f, etc)

- **le mode tracé de courbes en fonction de la fréquence** : le VNA 1201SA permet de visualiser les valeurs du paramètre S11, de l'impédance, de la partie réactive de l'impédance, de la partie résistive en fonction de la fréquence.

Un troisième écran permet de régler la longueur du câble utilisé.

Malgré le prix modeste de cet appareil (<130 €), les grandeurs mesurées en mode point (cf image ci-dessous), telles que l'impédance, le rapport d'onde stationnaire (ROS ou VSWR en anglais) sont très comparables aux résultats que l'on obtiendrait avec des appareils achetés beaucoup plus chers.

:memo: **on parle aussi quelquefois d'analyseur d'antenne**, bien que l'appareil puisse être utilisé pour autre chose.


## Notions importantes pour ce TP

:warning: on supposera que l'on est en régime sinusoïdal est que la source à une impédance supposée de 50Ω.

### Notion d'ondes stationnaires

Dans ce cas, sur la ligne composée de la source (fournie par le VNA) et le récepteur (la charge que l'on branche sur le VNA), il existe une onde incidente de tension d'amplitude Vi et une onde réfléchie de tension  d'amplitude Vr (on pourrait raisonner de même avec une onde de courant).

Plus précisément, c'est la superposition de toutes les ondes allant dans un sens -en tenant compte des éventuelles multiples réflexions entre source et charge- avec la superposition de tout ce qui va dans l'autre sens qui se superpose et peut donner lieu à une onde "plus ou moins stationnaire".

Comme ce sont les ondes de tension que l'on observe à chaque instant sur la ligne on définira un rapport d'onde stationnaire en tension, ou dans certains cas un taux d'onde stationnaire en tension.

### ROS ou Rapport d'ondes stationnaires en tension 

Entre la source et la charge, coexistent une onde **incidente**, d'amplitude Vi, et une onde réfléchie, d'amplitude Vr.

La superposition de ces deux ondes, va engendrer une onde résultante dont l'amplitude va varier le long de la ligne et au cours du temps (la stationnarité n'est jamais parfaite entre source et réccepteur).

Les interférences produites par la superposition de ces deux ondes donnera, en certains points une amplitude maximale de valeur Vmax = Vi + Vr (interférences constructives), et en d'autres points une amplitude minimale Vmin = Vi -Vr (interférences destructives).

![définition du ROS](./ressources/ROS_definition.png)


Remarque : **ROS en anglais donne V pour tension + standing wave ratio SWR = VSWR**, valeur affichée par le VNA.


### Coefficient de réflexion Γ

On définit le *coefficient de réflexion Γ* comme le rapport des amplitudes des amplitudes des ondes de tension réfléchies et incidentes.

Soit : ![définition de Γ](./ressources/GAMMA_definition.png)


![relations entre Γ, ρ et ROS ](./ressources/relations_gamma_rho_ROS.png)


### TOS ou taux d'onde stationnaire

:warning: C'est une grandeur souvent mal comprise et son usage est déconseillé.

Le taux d'onde stationnaire, noté TOS est défini par **TOS = 100 ρ**.  C'est un pourcentage.

    Les relations entre ROS, TOS et ρ sont à établir dans la préparation.


### Relations entre pertes de retour et ROS (ou VSWR prononcé "viswar")

Le tableau ci-dessous donne des ordres de grandeur qui permettront de se familiariser avec les valeurs affichées par le VNA

| pertes de retour en dB | ce que cela signifie |  nombre VSWR (ou ROS)| Commentaire |
|:-----------:|:----------------------------------------------------------------:|:---------:|:-------------:|
|  0 dB | 100% de réflexion, la charge ne reçoit rien, et tout est réfléchi | infini | très mauvais |
|  1 dB | 80% de l'énergie est réfléchie, la charge reçoit 20%    | 17   | très mauvais |
|  2 dB | 63% de l'énergie est réfléchie, la charge reçoit 37%    |  9   | très mauvais |
|  3 dB | 50% de l'énergie est réfléchie, la charge reçoit 50%    |  6   | mauvais |
|  5 dB | 32% de l'énergie est réfléchie, la charge reçoit 68%    |  3,5 | limite |
|  6 dB | 25% de l'énergie est réfléchie, la charge reçoit 75%    |  3   | limite |
|  **8 dB** | **16% de l'énergie est réfléchie, la charge reçoit 84%**    |  **2.3** | **acceptable** |
| **10 dB** | **9% de l'énergie est réfléchie, la charge reçoit 90%**    |  **2**   | **bon** |
| **20 dB** | **1% de l'énergie est réfléchie, la charge reçoit 99%**    |  **1.22** | **très bon** |
| **30 dB** | **0,1% de l'énergie est réfléchie, la charge reçoit 99,9%**    |  **1.07** | **excellent** |
| **40 dB** |**0,01% de l'énergie est réfléchie, la charge reçoit 99,99%**  | **1.02** | **excellent** |


![Relation ROS - ρ - energie](./ressources/relation_ROS_ρ_energie.png)

**En résumé: si le VSWR est inférieur à 2, la charge est supposée adaptée.**



## Prise en main de l'analyseur d'antenne vectoriel VNA 1201SA

![VNA 1201SA](./ressources/VNA1201SA.png)


### Précaution d'utilisation

:memo:  **Avertissement :** Cet appareil est un appareil bon marché, mais **fragile**. Il est toutefois suffisant pour les objectifs du TP dès lors que la calibration a été réalisée avec rigueur et application, et il va de soi que **toutes les manipulations seront réalisées avec un maximum de soin**.


:warning: **le VNA 1201SA est sensible à son environnement**. En particulier, pour chaque mesure, il faut respecter les consignes suivantes :

- se placer loin des murs pour faire les mesures ;

- faire reposer l'appareil sur un support non metallique (par exemple il repose sur une table en bois) ;

- se placer loin de toute source lumineuse ou de toute source de rayonnement électromagnétique dans le cas général ;

- ne pas toucher l'appareil, ni les câbles, ni l'antenne (si la charge est une antenne) ;

- utiliser, si possible, des gants anti décharges électrostatiques pour toute manipulation.


:memo: **N.B.** : dans le cas d'une mesure sur une antenne en situation, il est d'usage d'utiliser un montage permettant d'assurer l'isolation électrique entre antenne et mesure (non abordé dans ce TP).


### Commandes simples

**Mise en marche** : Appuyer sur CTRL, et sans relâcher, appuyer sur OK. Lorsqu'on relâche les 2 boutons, l'appareil se met en fonctionnement.

**Arrêt** : appuyer sur OK pendant plus de 2s, puis relâcher le bouton : l'appareil s'arrête de fonctionner.

**Domaine fréquentiel accessible à la  mesure** : de 34,375 MHz à 2,7 GHz

**Molette** : permet de changer les valeurs lorsque le champ sélectionné est actif (paramètre entouré d'un cadre jaune), sinon permet de passer d'un champ à l'autre (paramètre apparaît en rouge).

**Calibration** : appuyer sur CTRL + M deux fois pour passer en mode calibration. Un appui sur M pour en sortir (dans ce cas, on retourne dans le mode point de mesure)


### Réglages en mode point de mesure

L'affichage en mode "point de mesure" correspond à celui de l'analyseur 1201SA en début de ce texte, c'est à dire permettant de faire une mesure à une fréquence donnée.

**On passe à ce mode après un ou plusieurs appuis sur M** (en fonction de l'état précédent)


#### Réglage de la fréquence du point de mesure

- un appui sur flèche gauche (  <-- ) ou flèche droite ( --> ) permet de changer la valeur du digit sélectionné pour le réglage de la fréquence à laquelle on fait la mesure ;

- lorsque le digit choisi est sélectionné (il apparaît en rouge), la molette permet d'augmenter (rotation sens trigonométrique vu de dessus) ou diminuer (rotation sens horaire vu de dessus) la valeur de ce digit ;

- pour le réglage des fréquences, le premier digit correspond en fait au nombre de centaines de MHz, et on peut régler de rien (pour f = 34MHz) à 27 lorsqu'on atteint 2,7 GHz (fmaxi).

### Réglages en mode tracé de courbes

#### Paramètres réglables

Les paramètres réglables dans ce mode sont :

- le type de courbe (impédance Z, réactance X, résistance R, coefficient de répartition S11 = 20 log |Γ| (Γ coefficient de réflexion), le rapport d'onde stationnaire VSWR) ;

- le facteur d'échelle (0.1, 0.2 , 0.5, 1, 2, 5, 10)

- la fréquence du point sur la courbe ;

- la fréquence de début de balayage et celle de fin(i.e. la bande de fréquence étudiée). **La valeur peut être ajustée par digit, et les valeurs limites basse et hautesont [respectivement] 34 MHz et 2.7 GHz

#### État d'un paramètre réglable.

Tout paramètre ajustable (cf paragraphe précédent) peut avoir deux états : sélectionné ou modifiable.

- un appui sur la molette permet, pour le paramètre en cours de sélection, de passer du mode **modifiable (en jaune)** au mode **juste sélectionné (en rouge)**

- **lorsqu'un paramètre est en mode sélectionné**, *une rotation de la molette permet de passer au paramètres suivant*.

- **lorsqu'un paramètre est en mode modifiable**, *une rotation de la molette permet de modifier sa valeur*


## Préparation du TP

**Buts du TP** :

- [x] Réaliser une procédure de test permettant d'étalonner l'analyseur de réseau vectoriel ;
- [x] mesurer le comportement de l'impédance d'un câble de longueur 3m, lorsqu'il est fermé sur son impédance caractéristique entre 0,34 et 3 GHZ ;
- [x] tracer la courbe de réponse d'une antenne wifi (normalement accordée sur 2,4GHz) et analyser les résultats ;
- [x] rédiger la fiche recette associée à l'étalonnage du VNA.

![**Illustration : connecteur SMA (partie femelle)**](./ressources/connecteurs_RF_SMA.png)

*Illustration : connecteur SMA (partie femelle)*


Liste du matériel utilisé dans ce TP :

- Analyseur d'impédance vectoriel NanoVNA 1201SA ;
- carte de tests (Kit de démo RF NanoVNA RF -avec adaptateurs SMA IPX- module de test Vector Network) ;
- fiches de test SMA (Open, Short et Load = 50 Ω) ;
- un câble coaxial, d'impédance caractéristique à déterminer, de longueur 3m, avec fiches SMA ;
- différents connecteurs, associés (pour faciliter le rangement, mais le but n'est pas là) ;
- une antenne à analyser : antenne Yagi (par exemple une de celles utilisées pour le TP TNT) ou une antenne LoRa ou encore une antenne de Dongle Zigbee

N.B. : pour certaines antennes, il pourra être utile de demander un adaptateur RP SMA supplémentaire au professeur pour faire la mesure.

**Travail demandé** :

- **répondre aux questions ;**

### Questions sur l'appareil


En utilisant la ressource appelée [**NIEI_VNA1201SA_technical_features.png**](./ressources/NIEI_VNA1201SA_technical_features.png), répondre aux questions ci-dessous :

**Q1 : rappeler la relation entre la longueur d'onde, la fréquence et la célérité des ondes électrromagnétiques**

**Q2 : dans quelle bande de fréquence le VNA 1201SA permet-il de faire des mesures ?**

**Q3 : donner le domaine des longueurs d'ondes étudiées avec cet appareil de mesure**

**Q4 : est-il possible de se connecter à cet appareil via un des 2 ports USB pour faire des mesures ?**


### Questions sur le fonctionnement de l'appareil


**Q5 : comment met-on cet appareil en route ?**

**Q6 : comment l'arrête-t-on ?**

### Questions sur les définitions


**Q7 : rappeler les conditions d'adaptation d'impédance en courant continu entre une source (notée Rs) et sa charge (notée R)**

**Q8 : dans quelle condition le transfert maximum en puissance est-il réalisé ?**

**Q9 : En utilisant la notation complexe, rappeler la définition d'une impédance. Rappeler les expression des impédances d'une résistance, d'un condensateur et d'une bobine.**

**Q10: en vous inspirant des informations données dans ce texte, retrouver les relations entre ROS, TOS et ρ**

### Questions sur la calibration du VNA

**Q11: Comment reconnait-on chacune des 3 terminaisons ?** (un vrai raisonnement est attendu)

**Q12:  à quoi servent les adaptateurs non nommés ?** (i.e. question précédente)


    Cette préparation devra être rendue en tout début de TP.


## Manipulations

Le travail demandé est présenté ci-après. Il est recommandé de lire attentivement les consignes et de suivre pas à pas la démarche proposée.


### Réaliser la calibration du VNA

C'est une étape essentielle car la qualité des mesures en dépend. **Penser à prendre le plus de notes possibles pendant la manipulation car elles seront utiles à la rédaction du compte rendu, qui est à rendre AVANT la fin du TP (soit juste avant le rangement du matériel).**


### Maîtrise des différents écrans et des possibilités du VNA

En utilisant la notice, faire l'inventaire des posssibilités offertes par cet appareil (ce point sera repris dans le cours sur les antennes)


### Présentation du kit de calibration

Avec le VNA sont fournis les éléments suivants :

- 3 terminaisons, dont on sait que l'une d'elles est O, une autre S, et la dernière une impédance de 50 Ω

- un câble de 3m qui servira à réaliser quelques mesures

- d'autres adaptateurs dont on ne précise pas le rôle



![Kit de calibration avec connecteurs de type SMA](./ressources/VNA_calibration_kit.png)

*Kit de calibration avec connecteurs de type SMA*


En appuyant le bon nombre de fois sur M, faire apparaître le réglage du câble, et régler sa longueur sur 1m (valeur minimale).

:warning: *On fera l'hypothèse que cela ne perturbe pas la calibration*

Une fois ce réglage préliminaire fait, suivre les étapes suivantes :

- en vous aidant de la notice et des explications, mettre le VNA en mode calibration ;

- en appuyant le bon nombre de fois sur M, faire apparaître le réglage du câble, et régler sa longueur sur 1m (valeur minimale).

- quand vous pensez que c'est correct appelez le professeur en expliquant la démarche que vous avez définie.

Si votre plan est correct, commencez la calibration cf la méthode donnée ci-dessous;

N.B. : pour valider une étape (O, S ou load), il faut appuyer sur le bouton "flèche gauche".


La procédure de calibration se fait en 3 parties :

- on commence par brancher (en la vissant à la place du bouchon jaune, qu'on prendra soin de remettre à la fin du TP) la charge O ( = open pour circuit ouvert) ;
- quand le VNA signale que la calibration a atteint 100% pour la terminaison de type "circuit ouvert", on peut démonter cette charge et placer la suivante S ( = short, pour mesure en court-circuit) ;
- même procédure : dès que la calibration de cette terminaison est réalisée à 100%, on peut démonter la charge S, et placer la charge de 50 Ω.

La troisième mesure est identique aux deux autres, mais avec la charge adaptée de 50 Ω ("Load"). Quand elle se termine (affichage 100%), le VNA est calibré et les mesures peuvent commencer.


**Cette procédure est obligatoire et systématique (on ne peut pas faire de mesure fiable sinon).**


## Mesure de l'impédance et comportement fréquentiel d'un câble de longueur 3m.


On se place en mode point (cf photo du VNA + haut). Pour commencer, on branchera le câble directement sur le VNA, et on utilisera la terminaison de 50Ω. Faire varier la fréquence du signal fourni par le VNA de 0,34 à 2.7 GHz. Que constatez-vous ?  (il faut justifier la réponse)

**:memo:** : pour cette mesure, on notera l'impédance (partie réelle et partie imaginaire) du câble pour chaque fréquence appliquée.


Recommencer la mesure avec la terminaison O (circuit ouvert). Que constatez-vous ?


Peut-on retrouver la longueur du câble avec le VNA ? Peut-on améliorer la mesure ?

Pour terminer, on pourra essayer de faire quelques mesures avec la carte TEST. Attention : il faut beaucoup de soin pour réaliser les connexions, car les fiches IPX sont **très fragiles**.

En utilisant la carte TEST, vérifier que les résultats obtenus sont conformes aux attentes. 

:memo: on ne demande pas d'expliquer le fonctionnement de l'Abaque de Smith, mais simplement de comparer les courbes obtenues à celles proposées sur la carte.

## Comportement fréquentiel d'une antenne

Pour cette partie, on utilisera l'antenne fournie au début du TP, en utilisant l'adaptateur éventuel permettant de la connecter au VNA (VHF -> SMA ou IPX -> SMA).

**Travail demandé**

En affichant la caractéristique en fonction de la fréquence, quelle est la fréquence pour laquelle Z est la plus faible ? Que peut-on dire du signe de la partie réactive ?

Comparer la fréquence de résonance obtenue avec celle attendue. L'antenne est-elle conforme ?

Si le temps le permet, une antenne YAGI est disponible. Faire la même mesure et comparer les résultats obtenus. Cette seconde antenne présente-t-elle un défaut ? (le cas échéant : le décrire rapidement dans le compte rendu).

## Fiche recette et compte rendu

Réaliser les fiches recettes permettant :

- [x] de calibrer ce VNA ;

- [x] d'afficher la valeur des différents paramètres pour le câble de longeur 3m, à 1GHz ;

- [x] d'afficher les différentes caractéristique de ce câble entre 0.5 GHz et 2 GHz (ou pour une antenne qui sera fournie);

- [x] réaliser le compte rendu pendant la séance.


**Ce compte rendu devra être rendu à la fin du TP (envoyé sous forme numérique, format .pdf de préférence).**




**© Eric Bachard  Aout 2024 - Tous droits réservés**

