[Retour à la liste des TP](https://framagit.org/ericb/documents/-/tree/master/Physique/BTS2/Activites_experimentales/CoEnseignement)


# TP analyse de trames I2C avec l'Analyseur logique 8bits SALEAE

**INFORMATION IMPORTANTE : l'accéléromètre MMA8452A s'appelle VMA208 chez Vellemann (fournisseur du composant)**

**Thème de ce TP : Visualiser et étudier le fonctionnement du bus I2C, avec un microcontrôleur Arduino et un accéléromètre MMA8452A.**


## Préparation

Travail demandé :

- répondre aux questions ci-dessous
- Cette préparation devra être rendue en tout début du TP


**Q1** Que signifie I2C ?

**Q2** Combien de fils sont nécessaires pour réaliser le bus I2C en pratique ?

**Q3** Rappeler le fonctionnement d'un transistor bipolaire à collecteur ouvert, ou d'un transistor à effet de champ en Drain ouvert

**Q4** Quel est le rôle des résistances de tirage sur le bus I2C ? Est-ce un rôle important ?

**Q5** Rappeler la condition de Start et dessiner le chronogramme correspondant

**Q6** Rappeler la condition de Stop et dessiner le chronogramme correspondant

**Q7** Dans le fichier Example3_Orientation.ino, qu'est ce qui indique FORMELLEMENT que l'on utilise le bus I2C ?

**Q8** Puisqu'on utilise le bus I2C, pourquoi a-t-on besoin d'utiliser Serial.begin() ?

**Q9** À quoi sert la ligne Wire.begin() ?

**Q10** Quelle est la différence entre les instructions Wire.begin() et Wire.init() ?


Suggestion : la réponse à la question 10 se trouve dans le code


## Partie 1

### Buts de cette partie

- installer la ou les bibliothèques nécessaires (fournies sous forme d'une archive .zip dans le dossier ressources)
- écrire le programme ou modifier un programme Arduino fourni (des exemples sont fournis)
- réaliser un montage Arduino associé à des capteurs utilisant le bus I2C (accéléromètre MMA8452A par exemple)
- le compiler et valider le fonctionnement attendu 

### Travail demandé

#### Installation partie logicielle

Deux logiciels sont disponibles pour faire les enregistrements. Le premier est PulseView ([projet Sigrok](https://sigrok.org/wiki/Main_Page)), et permet de faire exactement la même chose (il fonctionne parfaitement). Cependant, un logiciel est fourni par la marque de l'analyseur logique utilisé, et il a été décidé de l'utiliser pour avoir une autre expérience.

Pour les autres TP portant sur les bus synchrones (I2C, SPI, CAN) que nous utiliserons, ou asynchrone (UART, One-Wire ou autre, cf le TP mesure de température et d'humidité), PulseView s'avère être une excellente solution. L'idéal est de maîtriser les 2.

Le logiciel fourni par [saleae](https://www/saleae.com) s'appelle Logic 2 (version 2.14 actuellement), et doit être téléchargé séparément (voir plus bas).

:memo: **Note:** Pour le reste, tout ce qui doit être installé ou être utilisé, est disponible dans le dossier [**RESSOURCES**](../ressources). D'autres ressources, comme l'aide à la configuration du bus I2C avec le logiciel Logic sont aussi présentes dans ce dossier.

Pour le fonctionnement de l'accéléromètre avec l'Arduino, télécharger l'archive SparkFun_MMA8452Q_Accelerometer.zip placée dans le dossier [**RESSOURCES**](../ressources), et la placer dans un dossier accessible de votre ordinateur.

Ouvrir l'IDE Arduino, et installer cette bibliothèque MMAQ8452A à l'aide de l'IDE Arduino :

Croquis -> Importer bibliothèque ...

Une fois la bibliothèque ajoutée, ouvrir l'exemple Nommé Example3_Orientation présent dans cette bibliothèque SparkFun_MMA8452Q_Accelerometer, en utilisant :

Fichier -> Carnet de Croquis -> SparkFun_MMA8452Q_Accelerometer -> Example3_Orientation 

Enregistrer ce fichier pour le rendre modifiable dans un dossier avec les droits en écriture.

N.B. : toutes les bibliothèques ajoutées par l'utilisateur, sont dans le dossier ~/sketchbook/libraries, et c'est une bonne habitude d'enregistrer les exemples dans le dossier ~/sketchbook.


Le programme Example3_Orientation.ino permettra, dans la console série, de connaître orientation du circuit intégré en temps réel (haut / flat / bas / droite / gauche)


#### Réalisation du montage

L'accéléromètre est un VMA208, entièrement compatible avec le MMA8452A de chez SparkFun.

**Face avant** : ![VMA208 face avant](../ressources/images/layout/VMA208_front.png)

**Face Arrière (image importante, car donne le brochage)**: ![VMA208 face arrière](../ressources/images/layout/VMA_208_back.png)


Réaliser le montage (cf schéma) en respectant 

- les couleurs des fils utilisés
- la connexion de l'accéléromètre : 5 broches sont à relier pour le montage à réaliser (cf brochage du MMA8452A fourni dans le dossier ressources)

Pour le VMA208, les broches à connecter sont, avec la couleur de fil à respecter :

- 5V        ROUGE
- 3,3V      ORANGE
- 0V (GND)  NOIR (ou VERT)
- SDA       JAUNE
- SCL       BLEU

**Les autres broches ne sont pas utilisées**


**FAIRE VÉRIFIER LE MONTAGE PAR LE PROFESSEUR AVANT DE BRANCHER L'ARDUINO À L'ORDINATEUR.**

#### Mise en oeuvre du programme

Brancher l'Arduino et vérifier qu'il est bien reconnu.

Compiler et téléverser le binaire exécutable dans l'Arduino et vérifier que le programme fonctionne comme attendu.

Dans le cas contraire, modifier le programme au besoin.

### Mesures

Ouvrir le moniteur série, et constater que le système fonctionne correctement.

IMPORTANT : pour la mesure qui suit, faire très attention à ne pas tout débrancher en bougeant l'Arduino + l'accéléromètre

Vérifier, en changeant l'orientation, que l'accéléromètre affiche le bon état (parmi : Up, Down, Left, Right, Flat)

Estimer le temps de réponse du circuit.

(à compléter)

## Partie 2

Buts :
- mettre en oeuvre un analyseur logique basique (Saleae 8 bits) ;
- utiliser le logiciel Logic 2.14 (Saleae, disponible pour Linux).
- relever les chronogrammes illustrant les échanges sur le bus I2C
- identifier le start, le stop, des données écrites, et lues 


![Analyseur logique 8 bits SALEAE](../ressources/images/SALEAE/analyseur_logique_vue_de_face.png)


### Travail demandé

#### Présentation de l'analyseur logique

Il s'agit d'un mini analyseur 8bits, qui coûte environ 13 €, et qui s'identifie comme un analyseur de marque [SALEAE]().

![Analyseur logique 8 bits SALEAE](../ressources/images/SALEAE/analyseur_logique_Saleae_10bits_reduit.png)

Pour l'utiliser, il suffit de connecter les broches utiles à la mesure au circuit sous tests d'un côté, et de l'autre, de brancher la fiche USB sur un port USB  de l'ordinateur. Pour tout ce qui suit, on utilisera le logiciel Logic 2.14, gracieusement fourni par [SALEAE]() et qui fonctionne sur les 3 OS majeurs. **Les fils et la correspondance des couleurs suivent.**

Attention : la numérotation commence à 1 sur l'analyseur (CH1 à CH8) , mais CH1 correspondra au canal 0 dans le logiciel)

| Nom du canal sur l'analyseur logique |  Couleur     | borne en contact côté Arduino    |
|:------------------------------------:|:------------:|:---------------:|
| **CH 0** | **ROUGE**   |     |
| **CH 1** | **MARRON**  |     |
| **CH 2** | **JAUNE**   | SDA |
| **CH 3** | **ORANGE**  |  |
| **CH 4** | **BLEU**    | SCL  |
| **CH 5** | **VERT**    |  |
| **CH 6** | **GRIS**    |  |
| **CH 7** | **VIOLET**  |  |
| **CLK**  | **BLANC**   |  |
| **GND**  | **NOIR**    | GND |


**On connectera donc le signal d'horloge SCL sur la broche CH4, et la masse sur GND**

Pour SDA, on prendra la couleur JAUNE, soit CH2.


Sur l'analyseur, on respectera la correspondance, et on pensera à cacher les canaux non utilisés, pour améliorer la visibilité (moins de courbes = plus de place pour aggrandir au mieux) et rendre l'analyse plus facile.

![Détail des connexions du VMA208 (le fil JAUNE, non présent, c'est pour le signal SDA)](../ressources/images/montage_Arduino/montage_VMA208_detail_cablage.png)

#### Installation du logiciel et prise en charge de l'analyseur logique SALEAE sous Linux (seule installation détaillée ici)

Télécharger le logiciel Logic 2.1x pour Linux (version actuelle 2.14), et téléchargeable ici : [Support SALEAE](https://support.saleae.com/logic-software/sw-download)

N.B. : il s'agit d'une application contenant tout. Il suffit de la rendre exécutable pour l'utiliser sans aucun problème de dépendances. Il existe aussi une autre version au format .zip, qui fonctionne de façon analogue, mais le lien pour la télécharger est différent (demander au professeur)

Brancher l'analyseur logique sur l'ordinateur, en utilisant un port USB.

Installer le logiciel et l'exécuter pour se familiariser avec son utilisation. 


Le montage complet devrait ressembler à :

![Montage complet, câblé et fonctionnel](../ressources/images/montage_Arduino/montage_avec_VMA208_vue_d_ensemble.jpeg)

#### Réglage du logiciel

Un document d'aide à la configuration du décodeur I2C est proposé [ici](../ressources/configuration_bus_I2C_avec_Logic_2.14/configuration_bus_I2C_avec_Logic2.pdf)

En particulier, il s'agira de :

- ajuster la couleur de l'interface utilisateur (Dark ou Light) ;
- régler quelques préférences ;
- vérifier que l'analyseur logique est vu par le logiciel (Device, en bas à gauche) ;
- insérer un contôleur I2C et le définir ;
- régler le temps d'acquisition à 10s (valeur plus raisonnable la valeur par défaut qui est de 100s) ;
- cacher les canaux non-utilisés
- déclencher un enregistrement

#### Exploitation du montage

Réaliser 2 enregistrements

##### Enregistrement 1

*Travail demandé*

**- Réaliser un enregistrement mettant en évidence l'initialisation du bus I2C ;**

**- Écrire une recette permettant de vérifier l'adresse du capteur sur le bus I2C.**

##### Enregistrement 2

*Travail demandé*

**Réaliser un enregistrement de maxi 10s, pendant lequel un élève enregistre, et l'autre change l'orientation du circuit, en faisant afficher plusieurs orientations de la plaque, faisant apparaître plusieurs positions ("Left", "Right", "Up", "Down", "Flat") dans le moniteur série. Attention à ce que les câbles restent bien enfichés.**

But : retrouver les échanges entre Arduino et VMA208, et vérifier que les données sont bien celles affichées. On pourra régler l'affichage pour que le décodeur fasse apparaître les données en décimal ou en hexadécimal pour faciliter l'analyse. Toutes copie d'écran illustrant cette analyse sera bienvenue. **Le but ultime étant de retrouver les informations et comprendre finement le fonctionnement de ce bus dans ce cas précis**


Après réglage du logiciel Logic, on devrait observer ce type de trace :

![Trace Arduino + VMA208 avec bus série I2C](../ressources/images/SALEAE/Logic_SALEAE_VMA208_trace_I2C.png)


## Synthèse

*Travail demandé*

**Rédiger le compte rendu en respectant les consignes**

Le compte rendu devra être rédigé pendant le TP, et envoyé au professeur juste avant la fin du TP.


    © Eric Bachard    14 août 2024.  Tous droits réservés



