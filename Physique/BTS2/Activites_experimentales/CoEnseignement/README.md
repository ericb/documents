# CO-Enseignement, TP abordés

## Liste des TP envisagés en BTS2 cette année :

 x = matériel reçu

- [x] [**TP utilisation d'un capteur composite (DHT11 taux humidité + température + son microcontrôleur)**](./Sujets/TP_etude_d_un_capteur_composite)
- [x] [**TP Analyseur de réseau vectoriel AAI N1201SA**](./Sujets/TP_VNA)
- [x] [**TP analyse de trames I2C avec l'Analyseur logique 8bits SALEAE (et le logiciel Logic 2)**](./Sujets/Trames_I2C/TP_analyse_trames_I2C_avec_Analyseur_8bits_SALEAE)
- [x] [**TP Analyse de trames I2C avec l'oscilloscope "I2C ready" (Siglent SDS1104X-E par exemple) associé à un Arduino**](./Sujets/Trames_I2C/TP_analyse_trames_I2C_avec_Siglent_SDS1004X-E)
- [x] [**TP Conversion Analogique Numérique (USB-DUX)**](./Sujets/TP_CAN/TP_USB-DUX)
- [x] [**TP programmation du Robot LEGO Spike + étude d'algorithmes simples**](Sujets/TP_Robot_LEGO_Spike)
- [ ] [**TP_mesure de distance avec capteur type "Time of Flight" type TF-Luna**](./Sujets/TP_capteur_temps_de_vol_TF-Luna)
- [ ] [**TP_mesure de distance avec capteur type "Time of Flight" type VL53LOX**](./Sujets/TP_capteur_temps_de_vol_VL53LOX_laser)
- [x] **TP Logique combinatoire**  (matériel ok et présent, texte à rédiger)
- [x] TP logique sequentielle
- [x] **TP Lidar Slamtec A2 M12** (matériel ok, logiciel prêt, texte à rédiger)
- [x] **TP Lidar Slamtec A1 M8**  (matériel ok, logiciel prêt, texte à rédiger)
- [ ] Trames_RS232_et_RS485
- [ ] TP Association diode Laser / Photorésistance
- [ ] TP modulations numériques
- [ ] TP LORA (modules zigbee ?)
- [x] TP Capteur RFID
- [ ] TP HALJIA TEC1-12710 Module de Refroidissement Peltier
- [ ] TP analyse de trames avec le bus CAN

Dans chaque répertoire, vous trouverez le texte du TP, ainsi que les ressources utiles (sauf si la taille est trop importante, mais dans ce cas, le logiciel sera téléchargé 1 fois et partagé localement).


    Auteur : © Eric Bachard / 12 août 2024. Tous droits réservés

