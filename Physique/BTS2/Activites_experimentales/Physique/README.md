Cette partie concerne la physique en BTS CIEL 2ème année. Auteur : Eric Bachard  / Août 2024

Les textes seront ajoutés au fur et à mesure. Dans chaque dossier, vous trouverez les textes des TP ainsi que des ressources adaptées (sauf si archive de taille trop importante).

Merci d'avance pour tout retour constructif.

# Liste des TP

:warning: **AVERTISSEMENT :**

    Cette liste est donnée à titre indicatif. Elle est non finalisée, et devrait évoluer dans le temps.

# Période 1

- [ ] [S1  TP Lignes de transmission](Sujets/TP_Lignes)
- [ ] [S2  TP Pont de Wheatstone](Sujets/TP_Pont_de_Wheatstone)
- [ ] [S3  TP Boucle de courant 4-20mA (balance électronique)](Sujets/TP_Boucle_Courant_4-20mA)
- [ ] [S4  TP transistor bipolaire en tout ou rien (TOR)](Sujets/TP_transistor_bipolaire_TOR)
- [ ] [S5  TP Redressement](Sujets/TP_redressement)
- [ ] [S6  TP modulation d'amplitude + analyse de Fourier (révisions)](Sujets/TP_AM_Fourier)

# Période 2

- [ ] [S7  TP Chaine de traitement du signal (partie acquisition, USB-DUX D)](Sujets/TP_Chaine_de_traitement_du_signal)
- [ ] [S8  TP Filtrage numerique](Sujets/TP_Filtrage_numerique)
- [ ] [S9  TP Acquisition audio](Sujets/TP_Acquisition_audio)
- [ ] [S10 TP Filtrage capacités commutées (MF10)](Sujets/TP_filtrage_capacites_commutees)
- [ ] [S11 TP Ondes mecaniques (Melde)](Sujets/TP_Ondes_mecaniques)
- [ ] [S12 TP capteurs ultrasonores (mesure distance)](Sujets/TP_capteurs_ultrasonores)
- [ ] [S13 TP modulations numériques](Sujets/TP_Modulations_numeriques)

# Période 3

- [ ] [S14 TP OScillateur à quartz](Sujets/TP_Oscillateur_a_quartz)
- [ ] [S15 TP accelerometre](Sujets/TP_accelerometre)
- [ ] [S16 TP oscillateur astable](Sujets/TP_oscillateur_astable)
- [ ] [S17 TP asservissement 1 : asservissement de vitesse + corrections](Sujets/TP_asservissement_vitesse)
- [ ] [S18 TP asservissement 2 : asservissement de position](Sujets/TP_asservissement_position)
- [ ] [S19 TP Polarisation de la lumière](Sujets/TP_Polarisation_lumiere)
- [ ] [S20 TP Antennes (mesures à l'aide d'un VNA)](Sujets/TP_Antennes)

# Période 4

- [ ] [S21 TP MLI et performances Arduino](Sujets/TP_MLI)
- [ ] [S22 TP Thermocouples (Effet Peltier)](Sujets/TP_thermocouples_Effet_Peltier)
- [ ] [S23 TP Camera IR HTi301 HT](Sujets/TP_Camera_IR_HTi301_HT)
- [ ] [S24 TP Photorésistance](Sujets/TP_photoresistance)
- [ ] [S25 TP capteur d'eclairement](Sujets/TP_capteur_d_eclairement)
- [ ] [S26  TP Filtrage analogique (Filter pro)](Sujets/TP_Filtrage_analogique)


**Ensuite, les projets commencent, et tout le temps sera alloué aux projets de 2ème année.**


(©) Eric Bachard  / 12 août 2024
Tous droits réservés
