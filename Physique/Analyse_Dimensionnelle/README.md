Dernier ajout : exercice 6 : Trinity, calcul de l'énergie dégagée par la bombe lors de l'explosion. // 10/23 ericb

L'analyse dimensionnelle est essentielle mais jamais formellement enseignée.

Le document mis à votre disposition est en cours de rédaction. Il sera complèté et amélioré progressivement.

Merci d'avance de signaler toute erreur et/ou imprécision (voir la page d'accueil pour la façon de procéder).

