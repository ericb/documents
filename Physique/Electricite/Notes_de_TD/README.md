#  Notes de TD PSA groupes A,B, K et L

Un grand MERCI à celles et ceux qui ont envoyé leurs notes de TD.

Ces documents sont à disposition pour les élèves souhaitant récupérer le TD.



## TD PSA du 22/09

Les notes de ce TD ont été réalisées par :

1. Madame Emma Tournis.

Documents concernés :

- 1 22-09-2020.pdf

- 2 22-09-2020.pdf

- 3 22-09-2020.pdf

## TD PSA du 29/09

Les notes de cd TD ont été réalisées par :

1. Madame Emma Tournis.

Documents concernés : 

- Notes de cours 1 29-09-2020.pdf

- Notes de cours 2 29-09-2020.pdf

- Notes de cours 3 29-09-2020.pdf

## TD PSA du 30/09

Les notes de cd TD ont été réalisées par :

1. Madame Margaux Rivollet (gpe A)

Documents concernés : 

- Notes-TD1-30.09.pdf


