# Travail à faire 

## Semaine du 21 au 25 septembre 2020


J'ai mis une nouvelle version (suite et fin) du chapitre 1 en ligne.

[**=> Lien**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Cours/Ch1_Introduction_Electrocinetique/Introduction_Electrocinetique_ericb.pdf)

Le chapitre 2 sera bientôt mis en ligne (étude des Réseaux linéaires)


Travail à faire pour cette semaine :

Cours:  lire, comprendre, apprendre et résumer le chapitre 1 en entier.
+ grouper les questions qui seront posées par la personne de votre choix (et qui me fera suivre vos questions tout le semestre)


TD : j'ai mis le TD 2 en ligne, ainsi que quelques corrections du TD1. 

[**=> lien**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre2_reseaux_lineaires/Chapitre2_reseaux_lineaires_ericb.pdf)

Travail demandé : faire ce que vous pouvez dans ce sujet, que l'on corrigera cette semaine

Attention : il y aura un petit contrôle de cours (CC1) en TD 


## Semaine du 28 septembre au 2 octobre 2020

**POUR TOUS LES GROUPES** : Contrôle de cours numéro 2  (environ 10 min au début du TD)**

Le contrôle portera sur la fin du cours 1 (introduction électrocinétique) + le cours 2 (réseaux linéaires)  jusqu'à la page 6 incluse (**jusqu'à la fin, caractéristiques des dipôles INCLUS**)

N.B. : la suite du cours sur les réseaux linéaires sera mis en ligne jeudi dans la journée;


**TD** : préparer tous les exercices possibles des sujets du [**TD1**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre1_introduction_a_l_electrocinetique/TD_ch1_introduction_electrocinetique_corrige.pdf) et du [**TD2**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre2_reseaux_lineaires/Chapitre2_reseaux_lineaires_ericb.pdf).


## Semaine du 5 octobre au 9 octobre 2020

**POUR TOUS LES GROUPES** :

- Contrôle de cours numéro 3 sur TOUS LES THÉORÈMES DES RÉSEAUX LINÉAIRES (environ 10 min au début du TD)**

- le chapitre 2 du cours (complet) est en ligne. Merci d'avance pour toute suggestion d'amélioration

- lire, comprendre et apprendre le cours sur le chapitre 2 (complet)

- les corrigés des CC2 sont en ligne

- j'ai mis en ligne les notes de cours de Madame Emma Tournis.


## Semaine du 12 octobre au 16 octobre 2020

**POUR TOUS LES GROUPES** : DS (durée 1h30) portant sur les réseaux linéaires en courant continu.

Principe : plusieurs petits exercices, de difficulté croissante.

Cours (dès que possible) : mise en ligne du chapitre 3 sur les régimes transitoires


## Semaine du 19 octobre au 23 octobre 2020



**Examen médian** : Pour des raisons de disponibilité des salles, il y aura 2 examens. Merci de bien noter le vôtre !

- **PSAA, PSAL le mercredi 21 octobre de 14h30 à 16h00 en salle P227**.

- **PSAB, PSAK le mercredi 21 octobre de 16h30 à 18h00 en salle P227**.

Principe :

- cours autorisé ;

- 2 parties distinctes : application directe du cours, puis exerices plus difficiles pour "explorer" ;

## Semaine du 2 novembre au 6 novembre

**TOUT EN DISTANCIEL : MERCI DE RESPECTER LES HORAIRES OFFICIELS AFIN DE GARDER LE RYTHME DE TRAVAIL**


**COURS** : [**Chapitre 3 Régime sinusoïdal permanent (et forcé)**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Cours/Ch3_Regime_sinuso%C3%AFdal_permanent/Chapitre3_regime_sinusoidal_force.pdf)

Ce document est prévu pour être lu, compris et assimilé en 2 semaines, voir 3 (maxi). Ensuite, chapitre 4 : régimes transitoires

Groupes K et L : lundi 2 novembre 14h30 à 16h
Groupes A et B : jeudi 5 novembre    8h à 9h30

TRAVAIL DEMANDÉ :

- lire, comprendre et apprendre le chapitre 3 (régime sinusoidal forcé) jusqu'à la page 16 incluses

- faire un résumé de cette partie du cours

Les élèves référents de chaque groupe sont invités à envoyer les questions (maxi 10), que je puisse y répondre en ligne (par écrit)

**TD** : [**Chapitre 3: valeur moyenne, valeur efficace et régime sinusoïdal forcé**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre3_regime_sinusoidal_force/Chapitre3_Regime_sinusoidal_force.pdf)

- Groupe K : mardi    3 novembre 16h30 à 18h

- Groupe L : mardi    3 novembre 14h40 à 16h

- Groupe A : mercredi 4 novembre 10h  à 11h30

- Groupe B : mercredi 4 novembre 8h30 à 10h

TRAVAIL DEMANDÉ : Faire les exercices 1 à 6 du document de TD sur le régime sinusoïdal permanent (la suite sera mise à jour régulièrement)

Sauf pour ceux ayant déjà une correction, je mettrai les corrections des exercices 1 à 6 en ligne en fin de semaine (vendredi 6)

Pour celles et ceux qui avancent vite : lire la seconde partie du cours, et préparer des questions.


**Bonne semaine à toutes et à tous, et n'hésitez pas à me contacter en cas de problème.**


## Semaine du 9 novembre au 13 novembre

**TOUT EN DISTANCIEL : MERCI DE RESPECTER LES HORAIRES OFFICIELS AFIN DE GARDER LE RYTHME DE TRAVAIL**

**COURS** : [**Chapitre 3 Régime sinusoïdal permanent (et forcé)**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Cours/Ch3_Regime_sinuso%C3%AFdal_permanent/Chapitre3_regime_sinusoidal_force.pdf)


Groupes K et L : lundi 9 novembre 14h30 à 16h

Groupes A et B : jeudi 12 novembre    8h à 9h30


TRAVAIL DEMANDÉ :

- lire, comprendre et apprendre le chapitre 3 (régime sinusoidal forcé): **TOUT LE DOCUMENT**.

- faire un résumé de cette partie du cours.

Les élèves référents de chaque groupe sont invités à envoyer les questions (maxi 10), que je puisse y répondre en ligne (par écrit)

**TD** : [**Chapitre 3: valeur moyenne, valeur efficace et régime sinusoïdal forcé**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre3_regime_sinusoidal_force/Chapitre3_Regime_sinusoidal_force.pdf)

- Groupe K : mardi    10 novembre 16h30 à 18h

- Groupe L : mardi    10 novembre 14h40 à 16h

- Groupe A : mercredi 11 novembre 10h  à 11h30  (**oui, le 11 novembre, on travaille aussi ^^^^**)

- Groupe B : mercredi 11 novembre 8h30 à 10h

TRAVAIL DEMANDÉ :

- le [**corrigé des exercices 1 à 6**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre3_regime_sinusoidal_force/Chapitre3_Regime_sinusoidal_force_corrige_ericb.pdf) est en ligne

- Terminer et reprendre avec le corrigé ce qui a posé problème avec les exercices 1 à 6 

- Faire les exercices 7 à 9 document de TD sur le régime sinusoïdal permanent (corrigé en fin de semaine)


**Bonne semaine à toutes et à tous, et n'hésitez pas à me contacter en cas de problème.**


## Semaine du 16 novembre au 21 novembre


**TOUT EN DISTANCIEL : MERCI DE RESPECTER LES HORAIRES OFFICIELS AFIN DE GARDER LE RYTHME DE TRAVAIL**



**COURS** : [**Chapitre 3 Régime sinusoïdal permanent (et forcé)**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Cours/Ch3_Regime_sinuso%C3%AFdal_permanent/Chapitre3_regime_sinusoidal_force.pdf)


Groupes K et L : lundi 16 novembre 14h30 à 16h

Groupes A et B : jeudi 19 novembre    8h à 9h30


Travail à faire pour cette semaine (16au 21 novembre)  

**Semaine d'approfondissement : pas de nouveauté, mais on prend le temps de bien comprendre**


- relire, comprendre, apprendre et résumer le chapitre 3 (TOUT)

- préparer une liste de 10 questions (maxi) par groupe, et transmises par l'élève référent du groupe. J'y répondrai rapidement ( = cette semaine)

- TERMINER le TD : cela signifie savoir faire -SANS regarder la solution - les exercices du TD sur valeurs moyennes, valeurs efficaces et le régime sinusoïdal forcé

Lien réponses aux questions : [**groupe B 12/11**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Reponses_aux_questions/reponses_questions_PSA12_groupe_B_2020_11_12.pdf)

Lien corrigé du TD (sera mis à jour le 17/11 dans la journée) : [corrigé du TD valeurs moyennes/efficaces + régime sinusoïdal forcé](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre3_regime_sinusoidal_force/Chapitre3_Regime_sinusoidal_force_corrige_ericb.pdf)

Pour ceux qui sont à l'aise, je vais remettre 3 ou 4 nouveaux exercices (avec corrigés) afin que vous puissiez vous entraîner.


**Prochain chapitre** : **partie 1 du cours sur le régime transitoire (circuits du 1er ordre)*.   => je le mettrai en ligne vendredi 20


EXAMEN : pour donner suite aux questions posées en privé, le final aura lieu en PRÉSENTIEL. À ce jour, il n'y a aucune ambiguïté sur le sujet, et je vais proposer une date bientôt. Dès que c'est officiel, je vous donne les informations.

J'aimerais aussi que les élèves référents me contactent rapidement ( 1 par groupe), pour discuter d'un autre sujet.




## Semaine du 23 novembre au 28 novembre

**TOUT EN DISTANCIEL : MERCI DE RESPECTER LES HORAIRES OFFICIELS AFIN DE GARDER LE RYTHME DE TRAVAIL**


**COURS** : Circuits du premier ordre en régime transitoire 

Groupes K et L : lundi 23 novembre 14h30 à 16h

Groupes A et B : jeudi 26 novembre    8h à 9h30


Travail à faire pour cette semaine (23 au 28 novembre) : 

- visionner les vidéos, comprendre et vous en faire un résumé, qui est à connaitre par coeur ;
- compléter ses connaissances avec le document en ligne dont le lien est donné ci-dessous.

Document complémentaire en ligne : [**Régimes transitoires, circuits du 1er ordre**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Cours/Ch4_Regimes_transitoires/Chapitre%20_4_complement_introduction.pdf)



**TD** : [**partie 1 : circuits du 1er ordre**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre4_regimes_transitoires/TD_Ch4_circuits_du_1er_ordre_en_regime_transitoire.pdf)

- Groupe K : mardi    24 novembre 16h30 à 18h

- Groupe L : mardi    24 novembre 14h40 à 16h

- Groupe A : mercredi 25 novembre 10h  à 11h30

- Groupe B : mercredi 25 novembre 8h30 à 10h



Travail demandé : tous les exercices sont à faire pour cette semaine.

Un corrigé sera mis en ligne en fin de semaine

**Bonne semaine à toutes et à tous, et n'hésitez pas à me contacter en cas de problème.**


## Semaine du 30 nomvembre au 4 décembre 


ATTENTION MODIFICATIONS TEMPORAIRES DE L'EMPLOI DU TEMPS (cette semaine, seulement)

Pour la suite, dans les 2 semaines qui viennent, nous allons tester une nouvelle forme de cours et TD :

MARDI APRÈS MIDI : nous essayons d'enregistrer des petites parties du cours et l'école les met en ligne  => les vidéos ne seront visibles qu'après le montage.

Pour NE PAS rien faire, profitez-en pour faire des exercices sur le sujet, apprendre le cours, préparer des questions ...


### Travail à faire et contenus


**COURS** : suite 1er ordre : décharge d'un condensateur, aspect énergétique

Groupes A, B, K et L : mardi 1er décembre dès que les vidéos sont disponibles  (en fin de journée, donc tout le monde peut les voir)

Consignes: visualiser les vidéos, comprendre et apprendre le cours associé, et préparer des questions à remettre aux élèves référents.


**Ce qui est prévu**:

Demain (mardi 1er décembre), le contenu prévu des petites vidéos :

- on repart de la fin de la charge d'un condensateurs pour le circuit RC, rappels
- étude de la décharge
- (peut-être) l'approche du circuit RC utilisé en intégrateur
- l'aspect énergétique

Semaine suivante : les circuits du 2ème ordre (partie 1 : mise en équations, équation caractéristique, cas possibles)


**TD** :

Le corrigé du TD4 (partie circuits du premier ordre) est en ligne :

Lien :  [**Corrigé TD circuits du premier ordre (partie 1)**](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/TD/Chapitre4_regimes_transitoires/TD_Ch4_circuits_du_1er_ordre_en_regime_transitoire_corriges_ericb.pdf)


=> Cette semaine, le mercredi  MATIN 2 décembre

De 9h à 10h30 Avec QUELQUES ÉLÈVES seulement (que j'aurai contacté la veille) : on fera quelques essais avec Big Blue Button, car je n'ai pas encore la maîtrise de l'outil.

Si ça fonctionne on prévient les autres et on fera des exercices (audio seulement). Méthode : je mets le sujet en ligne, pour tout le monde, et je corrige les exercices.


Points à aborder prochainement
- la modélisation des diodes dans les réseaux linéaires
- principe de l'étalonnage d'une sonde d'oscilloscope
- TD sur les circuits du 2ème ordre (partie 1)


**Pour quelques élèves seulement cette semaine** :

- Groupe A : mercredi 2 décembre 10h30  à 12h

- Groupe B : mercredi 2 décembre 9h00 à 10h30


Raison : je ne connais pas l'outil et on fera des essais (tout en répondant aux éventuelles questions sur le cours ou les exercices)



Pour ceux qui peuvent :

- Groupe K : (essai avec big blue button) jeudi matin 3 décembre de 9h00 à 10h30

- Groupe L : (essai avec big blue button) jeudi matin 3 décembre de 10h30 à 12h

Contenus et forme : pas entièrement défini pour ce jeudi.


La semaine prochaine, je vais essayer d'enregistrer les vidéos le lundi (en espérant que l'équipe technique sera disponible).

+ en parallèle, on ajoutera les cours et les corrections d'exercices (je m'enregistre en train d'écrire le corrigé, caméra par dessus mon épaule)

Je sais que -pour l'instant- cela bouscule l'emploi du temps le mardi, mais il faut savoir que pour enregistrer les vidéos de cours, je mobilise plusieurs personnes et il n'est pas possible pour l'instant de déplacer les enregistrements. Merci de votre compréhension.

Enfin, je vais voir s'il est possible d'avoir une visibilité plus grande de ces vidéos (visibles depuis l'extérieur par exemple)


N'hésitez pas à me contacter si vous avez des questions.


## Semaine du 7 décembre au 12 décembre 


Contenu :
- la leçon porte sur la première partie des circuits du second ordre
- elle sera prête pour les groupes A et B pour le cours du jeudi, et les groupe K et L peuvent la visionner à leur convenance (je ne peux pas faire mieux que l'enregistrer pour le lundi soir)


Pour les TD : on les fait avec BBB les jours et heures prévues, soit :

Sujet : circuits du 1er ordre (fin) + circuits du second ordre (plusieurs exercices que l'on corrige ensemble)

Fonctionnement : avec un groupe, j'enregistre une partie + le corrigé, et je fais un autre TD avec l'autre groupe. Ensuite, vous avec toutes les vidéos pour travailler.

Si j'arrive à tenir le rythme, cela fera 4 séances de TD avec corrigés et réponses aux questions, par semaine.


Important : je n'ai pas réussi à récupérer ce qui a été fait jeudi matin, mais je vais redoubler d'attention pour ne pas reproduire la même erreur demain et après-demain


**Mardi 8 Décembre** :

- Groupe L : mardi   8 décembre 14h30 à 16h ;

- Groupe K : mardi   8 décembre 16h30 à 18h ;

(merci de corriger si j'ai inversé les groupes)


**Mercredi 9 décembre**

- Groupe A : mercredi 9 décembre 10h  à 11h30 ;

- Groupe B : mercredi 9 décembre 8h30 à 10h.

Jeudi matin : les groupes A et B visionnent la leçon (de ~ 8h30 à 10h)

La remontée des questions par les élèves référents est toujours d'actualité, mais il est possible que je fasse une séance questions à la place.


N'hésitez pas à me contacter si vous avez des questions.


## Semaine du 14 décembre au 19 décembre 

Pour cette dernière semaine avant les vacances (ouf !) voici l'organisation. Merci de lire attentivement, car il y a des informations très importantes.

Contenu :

- la leçon porte sur la seconde partie des circuits du 2ème ordre

- elle sera prête pour les groupes A et B pour le cours du jeudi, et les groupe K et L peuvent la visionner à leur convenance (je ne peux pas faire mieux que l'enregistrer pour le lundi soir)

Merci à Mme Walgenwitz et à M. Dorvidal pour la réalisation de ces vidéos.


COURS : j'ai enregistré des petites vidéos (~ 2h) sur la dernière partie des circuits du 2nd ordre en régime transitoire cet après-midi.
        Les vidéos devraient être disponibles très prochainement.

Jeudi matin : les groupes A et B visionnent la leçon mise en ligne le lundi soir ou le mardi matin (de ~ 8h30 à 10h)


TD : afin que tout le monde puisse travailler à son rythme, j'ai mis en ligne les corrections des exercices 1 à 6 (il me reste à terminer le corrigé du 6, mais personne n'en est là.
     Lien : https://framagit.org/ericb/documents/-/tree/master/Physique/Electricite/TD/Chapitre4_regimes_transitoires/Second_ordre

Remarque : nous ferons ensemble tous ces exercices cette semaine et la semaine de la rentrée avec BBB.

Vidéos avec BBB : Mme Walgenwitz m'a montré que les vidéos sont accessibles : en bas dans "Enregistrements"

Il faut simplement rentrer dans la salle de votre groupe.

La bonne nouvelle : je crois qu'il est possible de les télécharger (mais j'ai un doute: je me demande si il ne faut pas certains droits)

Attention : sur la barre d'outils, à droite, il faut cliquer sur "l'oei" afin qu'il ne soit pas barré.

Fonctionnement : avec un groupe, je corrige des exercices, et vous participez le plus possible à l'élaboration de la solution.

**HORAIRES À RESPECTER** :

**Mardi 15 décembre**

- Groupe L : 14h30 à 16h ;

- Groupe K : 16h00 à 17h30 ;

(merci de corriger si j'ai inversé les groupes)

**Mercredi 16 décembre**

- Groupe A : 10h  à 11h30 ;

- Groupe B : 8h30 à 10h.


**EXAMEN** : il est prévu **EN PRÉSENTIEL**,

    N.B. : **sous réserve d'erreur ou de modifications liées à l'évolution de la situation sanitaire. 

    La convocation est prioritaire sur cette information**


    *    DATE       : le samedi 9 janvier 2021

    *    8 H - Convocation des Tiers temps de votre UV

    *    8 H 30 - Convocation de tous les étudiants de votre UV

    *    10 H Fin de l'épreuve

    *    10 H - 10 H 30 Pause - sans déplacement des étudiants et avec aération de la salle

    *    LIEU        :  Salle P108



**REPRISE DES COURS** : lundi 4 au vendredi 8 janvier :  le programme étant terminé, il n'y aura que des TD avec BBB au horaires prévus.

Enfin, si vous avez des questions, merci de les faire remonter par les élèves référents.


La remontée des questions par les élèves référents est toujours d'actualité, mais il est possible que je fasse une séance questions à la place.



N'hésitez pas à me contacter si vous avez des questions.


## Semaine du 4 au 9 janvier 2021

### Derniers changements

Approche numérique de la résolution d'une équation différentielle (2nd ordre) avec la méthode d'Euler

Voir corrigé partiel du [sujet de concours CCPINP 2019](https://framagit.org/ericb/documents/-/blob/master/Informatique/Python/Euler/second_ordre/MP_Physique1_CCP_2019_corrige_partie_info.pdf)


### Cours 



**Lundi 4 janvier** : 

- Groupes K et L : Révisions / correction exercices de TD (2nd ordren en régime transitoire) (de 14h30  à 16h)

**Jeudi 7 janvier**

- Groupes A et B : Révisions / correction exercices de TD (2nd ordre en en régime transitoire) (de ~ 8h à 9h30h)

Jeudi matin : les groupes A et B visionnent la leçon (de ~ 8h à 9h30h)

La remontée des questions par les élèves référents est toujours d'actualité, mais il est possible que je fasse une séance questions à la place.


N'hésitez pas à me contacter si vous avez des questions.


### TD

**Mardi 5 janvier** :

- Groupe L : mardi  5 janvier 14h30 à 16h ;

- Groupe K : mardi  5 janvier 16h30 à 18h ;

(merci de corriger si j'ai inversé les groupes)


**Mercredi 6 janvier**

- Groupe A : mercredi 6 janvier 10h  à 11h30 ;

- Groupe B : mercredi 6 janvier 8h30 à 10h.


### Examen final


**EXAMEN** : il est prévu **EN PRÉSENTIEL**,

    N.B. : **sous réserve d'erreur ou de modifications liées à l'évolution de la situation sanitaire. 

    La convocation est prioritaire sur cette information**


    *    DATE       : le samedi 9 janvier 2021

    *    8 H - Convocation des Tiers temps de votre UV

    *    8 H 30 - Convocation de tous les étudiants de votre UV

    *    10 H Fin de l'épreuve

    *    10 H - 10 H 30 Pause - sans déplacement des étudiants et avec aération de la salle

    *    LIEU        :  Salle P108





