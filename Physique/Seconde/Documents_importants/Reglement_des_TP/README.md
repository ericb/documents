Ce document est à signer par l'élève, et un représentant légal. C'est OBLIGATOIRE.

Il sera vérifié 15 jours (environ) après la rentrée.

L'élève devra l'avoir sur lui en TP, et ce document sera vérifié par le professeur.

Si ce document n'est pas signé, l'élève se verra refuser l'accès au TP (la note sera : 0), et il ira faire un travail complémentaire en permanence.
