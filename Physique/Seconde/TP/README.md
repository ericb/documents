## Informations importantes

Ce dossier sera mis à jour progressivement. Il contiendra des sujets des TP, ainsi que quelques documents qui pourraient être utiles à leur réalisation ou leur exploitation.

Malgré le soin apporté à la réalisation de ces documents, ils ne sont pas parfaits. Merci de nous rapporter toute erreur, faute de frappe etc.


Eric Bachard  / sept 2021


## Liste des TP

- TP1 : la verrerie et les pictogrammes de sécurité en chimie

- TP2 : détermination d'une concentration après dilution

- TP3 (à venir) : comment identifier les produits chimiques oubliés dans la pharmacie de ma grand mère ?
