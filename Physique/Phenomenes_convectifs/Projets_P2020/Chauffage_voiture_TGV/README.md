# Contenu de ce dossier :

* le sujet de l'étude : centrale_TSI_2019_physchim2.pdf

* Le corrigé : chauffage_voiture_TGV_ericb_mai_2020.pdf

* le rapport (partie 2 de l'étude) de Mme **Turban Lili**, M. **Aubert Théo** et M. **Abonnenc Lucas** : PS81_Dossier_convection_chauffage_d_une_voiture_de_TGV_version_finale.pdf

# Remontée d'informations, correction d'erreurs

Rien n'est parfait, et malgré le soin apporté à la rédaction de ces document, il reste probablement des erreurs.

Si vous trouvez ce que vous pensez être une erreur, ou quelque chose qui ne vous semble pas correct, vous pouvez me contacter par mail à prénom point nom @ free point fr. Cela sera corrigé au plus vite, et vous figurerez sur la liste des contributeurs (sauf si vous ne le souhaitez pas)

Enfin, **un retour sympa**, voir **un simple merci** aideront certainement à entretenir ce site plus facilement :-)

# Licence et droits d'utilisation

Ce dossier contient des documents qui, sauf mention explicite, sont sous Licence Creative-Commons by-sa qui est une licence libre et très permissive.
Cela signifie que vous pouvez télécharger, utiliser tout ou partie, modifier, partager ces documents à votre guise.
En contrepartie, cela implique que si vous les utilisez, vous devrez :

- **les partager sous une licence au moins équivalente (et libre)**

- **mentionner le nom de l'auteur de façon explicite**. (c'est la seule obligation, et elle permet à l'auteur de faire connaître son travail)

Remarque:  Ajouter un lien pointant vers le document original est fortement apprécié :-)

**Eric Bachard, le 16 juin 2020**
