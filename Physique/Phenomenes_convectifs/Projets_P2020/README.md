# PROJET PHÉNOMÈNES CONVECTIFS

## But de ces projets

Le but n'est pas de tout faire, mais de COMPRENDRE ce que l'on fait, et d'augmenter ses connaissances.

Délivrables : un document de 30 à 40 pages (maximum) portant sur 

- une étude théorique sur un des sujets proposés OU une étude originale, mais portant sur les contenus abordés.
- une étude ouverte, qui mettra en valeur votre façon de présenter cette étude.



## Organisation 

- travail par groupe (4 maximum)
- choix du sujet:  avant le 27 mai 2020.

N.B.: les sujets manquants seront ajoutés prochainement. (sinon, ce sera pour l'an prochain)


## Document final

- **DATE LIMITE POUR le RENDU : lundi 15 JUIN 2020 à minuit** (le 16 juin à 0h01 il sera trop tard)
- il devra être rédigé au format .pdf (avec les sources, au format .odt)
- le nom de tous les membres du groupe devront clairement apparaitre
- ENVOI : l'adresse mail que j'utilise pour vous envoyer le travail à faire. PAS CELLE DE l'ÉCOLE SVP

## Travail demandé

- partie 1 : rédiger les réponses aux questions pour un sujet choisi ensemble (le choix est définitif après le 23 mai)
- partie 2 : faire une étude "ouverte" sur le thème abordé

OU (cela dépend du sujet choisi, bien lire les consignes)
- partie 2 : étude logicielle (modélisation, etc) 
- en fonction des choix, il n'y a pas de partie 2 pour les sujets 9 et 16 par exemple

## 


# Sujets actuellement possibles :

## **[Sujet 1 : Convection naturelle](https://framagit.org/ericb/documents/-/tree/master/Physique/Phenomenes_convectifs/Projets_P2020/Convection_naturelle)**

Ce sujet s'inspire du sujet : Physique 1 CCP 2018 (PSI)

- Travail demandé : rédiger les réponses à un maximum de questions posées dans ce sujet.
- Complément : (à venir)
- Aide : document imprimé (sous forme de .pdf) du site : https://fr.wikipedia.org/wiki/Loi_d%27Ohm_thermique
- aide supplémentaire : texte de TP de MQ83, expliquant le fonctionnement de l'interféromètre de Michelson + la théorie des interférences.
- lien : https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/TP/MQ83_P2020_Cahier_de_TP_complet_ericb.pdf


## [**Sujet 2 : les échangeurs à plaques**](https://framagit.org/ericb/documents/-/tree/master/Physique/Phenomenes_convectifs/Projets_P2020/Echangeurs_a_plaques) 

Ce sujet s'inspire du sujet : Physique 1 CCP 2014 (PSI)

- Travail demandé : rédiger les réponses aux questions 1 à 21 incluse
- Complément : (à venir)
- Aide : sujet d'agrégation interne + autres documents à venir)

## **[Sujet 3 : Échangeurs et environnement](https://framagit.org/ericb/documents/-/tree/master/Physique/Phenomenes_convectifs/Projets_P2020/Echangeurs_et_environnement)**

Ce sujet s'inspire du sujet Centrale 2019 TSI épreuve de Physique-Chimie 2

- Travail demandé :
- rédiger les réponses des parties I (étude d'une pompe à chaleur idéale) et II (éviter les pertes
- partie 2 : faire une étude de l'impact sur l'environnement de telles applications (ou dérivées).
- Aide : (à venir)


## [**Sujet 4 : Étude théorique de la convection**](https://framagit.org/ericb/documents/-/tree/master/Physique/Phenomenes_convectifs/Projets_P2020/Etude_theorique_de_la_convection) 

D'après le sujet posé à l'agrégation externe de Physique 2010, option Physique.

- Travail demandé : rédiger les réponses aux questions 1 à 20 incluse. Ne rédiger que ce qui est bien compris.
- Aide : le corrigé du sujet d'agrégation 2010 est fourni. Mais il ne faudra traiter que ce qui est maîtrisé.

