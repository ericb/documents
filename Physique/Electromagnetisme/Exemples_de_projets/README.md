# PROJET ÉLECTROMAGNÉTISME

## Organisation 

- travail par groupe (4 à 6 maximum)
- choix du sujet:  avant le 23 mai 2020. Passé cette date, je constitue les groupes moi-même.

N.B.: les sujets manquants seront ajoutés prochainement. (sinon, ce sera pour l'an prochain)


## Document final

- **DATE LIMITE POUR le RENDU : lundi 15 JUIN 2020 à minuit** (le 16 juin à 0h01 il sera trop tard)
- il devra être rédigé au format .pdf (avec les sources, au format .odt)
- le nom de tous les membres du groupe devront clairement apparaitre
- ENVOI : comme pour le médian

## Travail demandé

- partie 1 : rédiger les réponses aux questions pour un sujet choisi ensemble (le choix est définitif après le 23 mai)
- partie 2 : faire une étude "ouverte" sur le thème abordé

OU (cela dépend du sujet choisi, bien lire les consignes)
- partie 2 : étude logicielle (modélisation, etc) 
- en fonction des choix, il n'y a pas de partie 2 pour les sujets 9 et 16 par exemple

## 


# Sujets actuellement possibles :

## **[Sujet 1 : Train à lévitation magnétique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S01_Train_a_levitation_magnetique)**

- Travail demandé : rédiger les réponses aux questions 1 à 30 du sujet X MP PSI 2009
- Complément : article BUP 2008 sur la lévitation
- Aide : sujet + corrigé PS26 2018 (exercice 2 lévitation magnétique)


## [**Sujet 2 : ligne de transmission**](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S02_Ligne_de_transmission) (thème : impédance caractéristique, propagation d'ondes, réflexion aux extrémités)

- Travail demandé : rédiger les réponses à toutes les questions de la partie 1 (jusqu'à la question 12 incluse.
- Si partie 2 traitée (fibre optique) cette partie dispense de l'étude ouverte sur le thème des lignes de transmission.
- si partie 2 non traitée, la partie 2 est à faire


## **[Sujet 3 : Haut parleur électrodynamique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S03_Haut_Parleur_Electrodynamique)**

- Aide : sujet + corrigé final PS21 P2014
- Travail demandé : rédiger les réponses aux question A1 et A2  (A3 : facultative, mais intéressante)

## **[Sujet 4 : Moteur à courant continu](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S04_Moteur_a_Courant_Continu)**

- Aide : sujet + corrigé final PS21 P2018
- Travail demandé : à définir précisément

## **[Sujet 5 : Transformateur torique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S05_Transformateur_torique)**

- Travail demandé : rédiger les réponses aux questions pour les parties II, III et IV (ne pas traiter la partie I)

## **[Sujet 6 : Pince ampèremétrique]()**

## **[Sujet 7 : Réseaux de diffraction](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S07_Resaux_de_Diffraction)**

Sujet (avec réponses) écrit par **M. JR Seigne** du lycée Clémenceau (Nantes). Site: http://www.seigne.free.fr/index.html

Travail demandé : les réponses sont données, mais il faut les rédiger correctement.

Consigne : ne faire QUE ce que vous comprenez. Cette étude sera obligatoirement assortie d'une "étude ouverte" sur l'utilisation des réseaux de diffraction.

N.B. : il manque un document que j'ai écrit il y a longtemps, et que je n'ai pas numérise : n'hésitez pas à me demander cette version numérisée en cas de besoin.

## **[Sujet 8 : Moteur électrostatique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S08_Moteur_Electrostatique)**

Document rédigé par **[M. Gérald Phillipe](http://gerald.philippe.free.fr)**

Travail demandé : rédiger les réponses à toutes les questions (pages 2 à 8).


## **[Sujet 9 : Équation de Poisson](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S09_Equation_de_Poisson)**

Sujet de type Physique + Informatique, il aborde la modélisation de certains systèmes physiques, utilisant l'équation de Poisson.

La partie informatique si elle est traitée, dispense de la partie 2 du projet. Si elle n'est pas abordée, on doit faire la partie 2 (étude "ouverte" sur le thème).
But: traiter plusieurs examples d'application de l'équation de Poisson.

Travail demandé (au choix):

- tout traiter (physique et informatique, avec python par exemple)
- ne faire que la partie physique, à compléter avec une étude (ouverte sur le thème des applications utilisant l'équation de Poisson)

## **[Sujet 10 : Géomagnétisme](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S10_Geomagnetisme)**

- Aide : Le sujet est un DS **écrit par Madame Françoise Lachize** professeur au lycée Saint Louis (Paris), pour la classe MP* 1. Il est tiré de Centrale PC 2016. Site : http://flachize.e-monsite.com/
- Travail demandé : rédiger le maximum de réponses, en vous aidant du sujet+corrigé donné dans l'aide. 

Consigne : ne faire que ce que vous comprenez bien, et signalez ce qui vous semble mériter un complément en cours.

## **[Sujet 11 : Circuit magnétique d'un moteur](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S11_Circuit_Magnetique_d_un_moteur)**

TODO : retrouver mes documents ...
- j'ai donné ce sujet ily a quelques années, en Prépa Agrégation (génie électrique). Très progressif et très appliqué, il permet de faire une comparaison entre les circuits élctriques linéaires les circuits magnétique linéaires.
- plutôt orienté PS21

- Travail demandé : rédiger les réponses aux questions.

## **[Sujet 12 : Machines synchrones et asynchrones](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S12_Machines_Synchrones_Asynchrones)**

(à venir)

## **[Sujet 13 : L'auto-trasnformateur](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S13_Autotransformateur)**

- Travail demandé : tout le sujet est à traiter


## **[Sujet 14 : Courants de Foucault](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S14_Courants_Foucault)**

- Travail demandé : rédiger les réponses à toutes les questions des parties 1.A, 1.B et 1.C

## **[Sujet 15 : La foudre](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S15_La_Foudre)**

2 choix sont possibles : 

- Choix 1 : tout traiter jusqu'à la question 31 incluse dispense de faire la partie "étude ouverte".
- Choix 2 : rédiger les réponses aux questions allant de 10 à 31 incluses + 2nde partie dite "étude ouverte" qui devra figurer dans le rapport.

- Travail demandé: rédiger les réponses aux questions
- Aide : sujet + **[corrigé du final PS21 P2017](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2017/PS21_P2017_final_ericb_corrige_V3.pdf)**
- **[le cours d'électrostatique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Cours/CH2_electrostatique)**

## **[Sujet 16 : Accéléromètre électrostatique](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S16_Accelerometre_Electrostatique)**

Ce sujet est tiré de X-ENS 2013. Il dispense de la partie 2 car il demande la mise en équation d'un oscillateur mécanique en remplacement

But : modéliser l'oscillation d'un accéléromètre pendulaire, utilisé (par exemple) avec la console Nintendo WiiU (TM)

- Travail demandé : rédiger les réponses aux questions jusqu'à la question 22 incluse.
- Pré-requis : le cours de PSB.
- Complément de cours : **[le cours de MQ83 sur les oscillateurs](https://framagit.org/ericb/documents/-/tree/master/Physique/Physique_vibratoire/Cours)** peut grandement aider (les oscillateurs y sont étudiés aussi)
- Aide : **[les exercices corrigés et les sujets d'examen corrigés de MQ83](https://framagit.org/ericb/documents/-/tree/master/Physique/Physique_vibratoire)**
