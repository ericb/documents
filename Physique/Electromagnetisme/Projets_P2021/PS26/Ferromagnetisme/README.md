## Travail demandé 

Parties à traiter entièrement : 

* parties I (étude expérimentale)  et II (influence des courants de Foucault sur un cycle d'hystérésis)

Seconde partie : III utilisation des matériaux ferromagnétiques

* III A (étude d'un circuit magnétique)

* III B (principe du disjoncteur différentiel)


Répondre à toutes les questions avec un maximum de clarté et de soin
