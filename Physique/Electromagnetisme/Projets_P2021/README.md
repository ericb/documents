# PROJETS P2021

## Organisation


- Travail par groupes de 4 étudiants (maximum) ;
- chaque groupe de 4 (exactement, sinon 3 pour le dernier groupe) me contacte pour me dire quel sujet il a choisi ;
- un sujet ne peut être pris qu'une seule fois ;
- premier arrivé, premier servi (2 groupes ont déjà choisi leur sujet !).
- tout les sujets proposés sont liés au programme de PS26, et les contenus abordés correspondent aux objectifs de l'UV

* partie 1 : théorique type devoir à la maison, sur un sujet d'exploration ;

* partie 2 : partie ouverte. Le groupe devra proposer une synthèse sur le même thème, voir un logiciel sur si l'opportunité se présente ;

* Chaque groupe devra rendre 2 parties distinctes sous forme de .pdf obligatoirement.


### partie 1 :

**La Partie 1 du projet est à rendre le 24 mai à minuit au plus tard**. Mardi 25 mai 2021 à 00h01 il sera trop tard (j'enlève 2 pts par jour de retard ensuite)

- le rapport contiendra les réponses aux questions, incluant les dessins, comme un devoir maison ;
- il devra être rédigé au format .pdf (avec les sources, au format .odt) ;
- il sera tenu compte de la qualité de la rédaction et de l'orthographe dans le barème ;
- le nom de tous les membres du groupe devront clairement apparaitre ;
- ENVOI : mon adresse mail @utbm.fr.


### Partie 2

Peut être commencée en paralèle et rendue de façon asynchrone. **Date limite : jeudi 8 juin minuit maxi**.

- le rapport contiendra une étude sur le même thème ;
- il devra être rédigé au format .pdf (avec les sources, au format .odt) ;
- le nom de tous les membres du groupe devront clairement apparaitre ;
- ENVOI : mon adresse mail @utbm.fr.


## Liste des projets proposés

- [x]  **S1  Alternateur de bicyclette**  // **attribué**
- [ ]  S2  Balance de Cotton + autre sujet en complément (boussole + champ magnétique terrestre) 
- [x]  **S3  Bobines de Helmholtz supraconductrices**  // **attribué**
- [ ]  S4  Bobine de Rumckhorf 
- [ ]  S5  Effet Hall 
- [x]  **S6  Ferromagnetisme**  // **attribué**
- [ ]  S7  Pince amperemetrique 
- [ ]  S8  Moteur_Electrostatique 
- [ ]  S9  Equation de Poisson 
- [x]  **S10 Oscilloscope cathodique** // **attribué**
- [x]  **S11 Geomagnetisme**   // **attribué**
- [x]  **S12 Autotransformateur**  // **attribué**
- [x]  **S13 Etude d'un transformateur** // **attribué**
- [x]  **S14 Transport et Stockage de l'électricite** // **attribué**
- [ ]  S15 Transformateur_torique 

choix du sujet:  avant le 30 avril 2021. Passé cette date, je constitue les groupes moi-même.

N.B.: les sujets manquants seront ajoutés prochainement. (sinon, ce sera pour l'an prochain)



## Travail demandé

partie 1 : rédiger les réponses aux questions pour un sujet choisi ensemble (le choix est définitif après le 23 mai)

partie 2 : faire une étude "ouverte" sur le thème abordé, OU (cela dépend du sujet choisi, bien lire les consignes), proposer un développement logiciel sur le même thème.




