# Thèmes des sujets posés aux examens finaux (par UV et par année)

### P2019

- [PS26 : propagation d'une onde plane électromagnétique + Courants de Foucault + Effet de peau](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2019/PS26/PS26_P2019_final_ericb_enonce.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2019/PS26/PS26_P2019_final_ericb_corrige_v4.pdf)

- [PS21 : Induction + Principe de l'alternateur](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2019/PS21/PS21_P19_enonce_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2019/PS21/PS21_P19_corrige_final_ericb.pdf)

### P2018

- [PS26 : calcul d'une f.e.m. induite + Phénomène de lévitation magnétique](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2018/PS26/PS26_P2018_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2018/PS26/PS26_P2018_final_exercice_2_ericb_corrige.pdf)

- [PS21 : Principe de la machine à courant continu bipolaire + étude d'une suspension électromagnétique](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2018/PS21/PS21_P2018_examen_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2018/PS21/PS21_P2018_examen_final_corrige_ericb.pdf)


### P2017

- [PS21 : Étude du champ électrique dans un nuage d'orage + champ tournant en régime sinusoïdal](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2017/PS21_P2017_final_ericb_V2.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2017/PS21_P2017_final_ericb_corrige_V3.pdf)


### P2016

- [PS21 : principe du galvanomètre magnéto-électrique](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2016/PS21_P2016_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2016/PS21_P2016_final_corrige_ericb.pdf)


### P2015

- [PS21 : Essai de freinage d'une luge d'été par induction](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2015/PS21_P2015_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2015/PS21_P2015_final_corrige_ericb.pdf)


### P2014

- [PS21 : Principe du haut-parleur électrodynamique + spire fixe dans un champ magnétique tournant](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2014/PS21_P2014_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2014/PS21_P2014_final_corrige_ericb.pdf)

### P2013

- [PS21 : freinage électromagnétique + freinage d'une masse](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2013/PS21_P13_final_ericb.pdf)   [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Finaux/P2013/PS21_P13_final_corrige_ericb.pdf)

