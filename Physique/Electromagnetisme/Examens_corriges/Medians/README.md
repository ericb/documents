# Thèmes des sujets posés aux examens médians en PS21 et PS26 (par UV et par année)

### P2021

- [Commun à PS21 et PS26 : principe du microphone électrostatique](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS26/PS26_median_P2020_enonce_ericb.pdf)   [  **(le corrigé)**  ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2021/PS21_PS26_examen_partiel_sujet2_corrige_ericb.pdf)

### P2020

- [PS26 : Étude d'un débitmètre pour eaux usées + lignes de champ et équipotentielles](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS26/PS26_median_P2020_enonce_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS26/PS26_median_P2020_corrige_ericb.pdf)

- [PS21 : Étude d'un précipitateur électrostatique + théorème de Gauss + calcul de flux](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS21/PS21_median_P2020_enonce_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS21/PS21_median_P2020_corrige_ericb.pdf)

### P2019

- [PS26 : Fil métallique de longueur infinie (E et B)+ Hexapôle électrostatique (d'après X 2005 PC partiel)](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2019/PS26/PS26_median_P2019_enonce_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2019/PS26/PS26_median_P2019_corrige_ericb.pdf)

- [PS21 : Principe mesure composante horizontale champ terrestre (classique, donné aussi au concours Mines MP 2019 partie 1)](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2019/PS21/PS21_median_P2019_enonce_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2019/PS21/PS21_median_P2019_corrige_ericb.pdf)

### P2018

- [PS26 : Modèle électrique de l'atmosphère + dipôle électrique + potentiel de Yukawa + Coquilles cylindriques coaxiales et chargées](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2018/PS26/PS26_P2018_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2018/PS26/PS26_P2018_median_ericb_corrige.pdf)

- [PS21 : Champ créé sur l'axe par 2 charges + sphère conductrice chargée](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2018/PS21/PS21_P2018_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2018/PS21/PS21_P2018_median_ericb_corrige.pdf)

### P2017

- [PS21 : Estimation de la distance Paris-Oslo + calcul de flux + distribution volumique de charge](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2017/PS21_P2017_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2017/PS21_P2017_median_corrige_ericb.pdf)

### P2016

- [PS21 : pendule à 3 fils chargé + cylindre métallique chargé en volume](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2016/PS21/PS21_P2016_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2016/PS21/PS21_P2016_median_corrige_ericb.pdf)

### P2015

- [PS21 : Pouvoir des pointes + Analogie entre interaction Gravitationnelle et interaction électrostatique](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2015/PS21/PS21_P2015_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2015/PS21/PS21_P2015_median_corrige_ericb.pdf)

### P2012

- [PS21 : Distribution linéique de charge + densité volumique de charge + coquilles conductrices chargées](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2012/PS21_P12_median_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2012/PS21_P12_median_corrige_ericb.pdf)

### P2006

- [PS21 : Condensateur plan et théorème de Coulomb avec diélectrique + Charges et lignes de champ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2006/PS26_median_P06_ericb.pdf)     [ **(le corrigé)** ](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2006/PS26_median_P06_corrige_ericb.pdf)
