# Quelques liens directs (par UV ou corrigés de sujets de concours)

Avertissement : tous les documents mis à disposition sur ce site le sont bénévolement, dans un but de partage de la connaissance. Ils ne sont pas parfaits, mais vous pouvez nous aider à les améliorer, par exemple en proposant une meilleure démonstration, ou en nous signalant qu'un calcul est faux (personne n'est à l'abri d'une erreur ou d'un oubli). En cas de problème de copyright, merci de me contacter rapidement ( Prenom point Nom @ free point fr ). Pour trouver ces coordonnées, il suffit de remonter d'un niveau.


Merci d'avance !

Eric Bachard

## Liens directs

Dernier ajout :

- [Centrale-Supélec TSI Physique-chimie 1, 2021, partie I : Gestion d’un digesteur anaérobie et valorisation des biogaz](https://framagit.org/ericb/documents/-/blob/master/Physique/Electricite/Complements/2021-Centrale-Supelec-Physique-Chimie_1-TSI-corrige.pdf)



- [Centrale-Supélec TSI Physique-chimie 2, 2019, partie II : dimensionnement du chauffage d'une voiture de TGV](https://framagit.org/ericb/documents/-/blob/master/Physique/Phenomenes_convectifs/Projets_P2020/Chauffage_voiture_TGV/chauffage_voiture_TGV_ericb_mai_2020.pdf)

- [Centrale-Supélec TSI Physique-chimie 2, 2018, parties : pompe à chaleur, échangeur, éviter les pertes](https://framagit.org/ericb/documents/-/blob/master/Physique/Phenomenes_convectifs/Projets_P2020/Echangeurs_et_environnement/Centrale_TSI_2018_PhysiChim2_corrige_ericb_mai2020.pdf)

- [CCP      PSI Physique-chimie    2015, partie : orage et foudre](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Exemples_de_projets/S15_La_Foudre)

- [CCP      TPC Physique           2018, partie : dissipateur thermique (principe, mesure du coefficient de transfert conducto-convectif)](https://framagit.org/ericb/documents/-/blob/master/Physique/Phenomenes_convectifs/Projets_P2020/Convection_naturelle/corrige_CCP2018_TPC_dissipateur_thermique_ericb_juin2020.pdf)

- [Centrale-Supélec TSI            2018, partie : analyse expérimentale des vibrations d'un verre](https://framagit.org/ericb/documents/-/blob/master/Physique/Physique_vibratoire/Medians/P2019/MQ83_P2019_examen_median_corrige_ericb.pdf)

- [Centrale Supelec PC             2015, partie : débitmètre pour eaux usées (Physique sujet 2), (sujet examen basé sur la partie 1)](https://framagit.org/ericb/documents/-/blob/master/Physique/Electromagnetisme/Examens_corriges/Medians/P2020/PS26/PS26_median_P2020_corrige_ericb.pdf)

- [Sujets + Corrigés d'examens de Physique (phénomènes convectifs, UV PS81)](https://framagit.org/ericb/documents/-/tree/master/Physique/Phenomenes_convectifs/Examens_corriges)

- [Sujets + Corrigés d'examens de Physique (électromagnétisme, UV PS26 et PS21)](https://framagit.org/ericb/documents/-/tree/master/Physique/Electromagnetisme/Examens_corriges)

- [Sujets + Corrigés d'examens de Physique vibratoire (UV MQ83)](https://framagit.org/ericb/documents/-/tree/master/Physique/Physique_vibratoire/Medians)

## Remontée d'informations, correction d'erreurs

Rien n'est parfait, et malgré le soin apporté à la rédaction de ces document, il reste probablement des erreurs.

Si vous trouvez ce que vous pensez être une erreur, ou quelque chose qui ne vous semble pas correct, vous pouvez me contacter par mail à prénom point nom @ free point fr. Cela sera corrigé au plus vite, et vous figurerez sur la liste des contributeurs (sauf si vous ne le souhaitez pas)

En particulier les corrections de certains sujets de concours d'entrée dans les grandes écoles d'ingénieurs ou corrigés d'examens, ou de TD de physique, sont proposés à titre gracieux, et votre retour -constructif- est très important. Dans ce cas, nous rappeler que pour ce type de sujet on répond plutôt de telle ou telle façon aidera beaucoup les élèves concernés, et vous pouvez aussi nous aider de cette façon là.

Enfin, **un retour sympa**, voir un simple **merci** de temps en temps aideront certainement entretenir ce site plus facilement :-)


## Licence et droits d'utilisation

Ce dépôt contient des documents qui, sauf mention explicite, sont sous **[Licence Creative-Commons by-sa](https://creativecommons.org/licenses/by-sa/2.0/fr/)** qui est une licence libre et très permissive. 

Cela signifie que vous pouvez télécharger, utiliser tout ou partie, modifier, partager ces documents à votre guise.

**En contrepartie, cela implique que si vous les utilisez, vous devrez** :

* les partager sous une licence au moins équivalente (libre)

* **mentionner le nom de l'auteur de façon explicite.**


