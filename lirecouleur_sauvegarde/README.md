Eric Bachard 31 12 2020

Ce dépôt est une copie des sources de Lire Couleur, une extension géniale utilisée dans OOo4Kids et OOoLight.

Site original et officiel : http://lirecouleur.arkaline.fr/

Protée : dépôt le plus ancien, devrait pouvoir être utilisé avec OOo4Kids

Master : pour Apache OpenOFfice 4


Licence : LGPL v2.1 (voir dans les sources, le texte y figure).

N'hésitez pas à leur poser des questions, car à l'origine, lire couleur était surtout prévue pour OOo4Kids.

Depuis que OpenOffice a été détourné par des malhonnêtes, on n'est même plus mentionnés.


En espérant vous avoir dépanné  :-)
